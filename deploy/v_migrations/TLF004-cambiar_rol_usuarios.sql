-- Deploy sc_vlci_userdb:v_migrations/TLF004-cambiar_rol_usuarios to pg

BEGIN;

UPDATE sc_vlci_userdb.public.granted_authorities
SET authority='AccUnifEconCiudMoviAlca' where username in('U301362','u301362','U18761','u18761', 'U18667', 'u18667', 'U13580', 'u13580');

COMMIT;
