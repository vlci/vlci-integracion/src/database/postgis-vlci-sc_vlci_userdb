-- Deploy sc_vlci_userdb:v_migrations/TLF032-alta-usuario to pg

BEGIN;

	insert into public.users (username, password, enabled, description, position) 
	values ('U813647', 'Password1', 1, 'Edurne Badiola', 'Funcionario OCI'),
	    ('u813647', 'Password1', 1, 'Edurne Badiola', 'Funcionario OCI');

	INSERT INTO sc_vlci_userdb.public.granted_authorities (username,authority) VALUES
		('U813647','DBPentDeveloper'),
	    ('u813647','DBPentDeveloper');

COMMIT;
