-- Deploy sc_vlci_userdb:v_migrations/1673-creacion-usuarios to pg

BEGIN;

update sc_vlci_userdb.public.granted_authorities set authority  = 'AccUnificadoEconomicoCiudadMovilidad' where username = 'U19316';

INSERT INTO sc_vlci_userdb.public.users (username,"password",enabled,description) VALUES
	('U18587','Password1',1,'Pilar De la Torre Fornes');

INSERT INTO sc_vlci_userdb.public.granted_authorities (username,authority) VALUES
	('U18587','AccUnificadoEconomicoCiudadMovilidad');

COMMIT;
