-- Deploy sc_vlci_userdb:v_migrations/TLF018-modificar_rol_economicoMovilidad to pg

BEGIN;

	UPDATE sc_vlci_userdb.public.granted_authorities
	SET authority='AccEconomicoMovilidad' 
	WHERE username IN (
	    'U19895', 'u19895'
	);

COMMIT;
