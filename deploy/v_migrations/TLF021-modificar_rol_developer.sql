-- Deploy sc_vlci_userdb:v_migrations/TLF021-modificar_rol_developer to pg

BEGIN;

	UPDATE sc_vlci_userdb.public.granted_authorities
	SET authority='DBPentDeveloper' 
	WHERE username IN (
	    'UPICOL', 'upicol',
	    'U18667','u18667'
	);

COMMIT;
