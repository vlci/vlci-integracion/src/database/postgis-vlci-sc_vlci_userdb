-- Deploy sc_vlci_userdb:v_migrations/2246-alta-usuarios-sabri-y-cecilia to pg

BEGIN;

insert into public.users (username, password, enabled, description, position) 
values ('UMAMAS', 'Password1', 1, 'Sabri Manai', 'Idrica'),
    ('umamas', 'Password1', 1, 'Sabri Manai', 'Idrica'),
    ('UFIROC', 'Password1', 1, 'Cecilia Fili Rodriguez', 'Idrica'),
    ('ufiroc', 'Password1', 1, 'Cecilia Fili Rodriguez', 'Idrica');

insert into public.granted_authorities (username, authority)
values ('UMAMAS', 'DBPentDeveloper'),
    ('umamas', 'DBPentDeveloper'),
    ('UFIROC', 'DBPentDeveloper'),
    ('ufiroc', 'DBPentDeveloper');

COMMIT;
