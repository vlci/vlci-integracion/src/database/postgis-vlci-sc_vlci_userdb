-- Deploy sc_vlci_userdb:v_migrations/TLF035-alta_usuario to pg

BEGIN;

insert into public.users (username, password, enabled, description, position) 
	values ('U813012', 'Password1', 1, 'Elena Ayllon Badia', 'Funcionario General/Director'),
    ('u813012', 'Password1', 1, 'Elena Ayllon Badia', 'Funcionario General/Director');

INSERT INTO sc_vlci_userdb.public.granted_authorities (username,authority) 
    VALUES
        ('U813012','AccCiudadMovilidad'),
        ('u813012','AccCiudadMovilidad');


COMMIT;
