-- Deploy sc_vlci_userdb:v_migrations/TLF027-cambio_rol to pg

BEGIN;

INSERT INTO sc_vlci_userdb.public.authorities (authority, description) 
	VALUES 
		('AccEconomicoMovilidadCiudad', 'Acceso a CdM Economico, Movilidad y Ciudad');


UPDATE public.granted_authorities
	SET "authority"='AccEconomicoMovilidadCiudad'
	WHERE username in (
        'U19895','u19895'
    );

COMMIT;
