-- Deploy sc_vlci_userdb:v_migrations/TLF029-alta-usuario to pg

BEGIN;

insert into public.users (username, password, enabled, description, position) 
	values ('U813624', 'Password1', 1, 'Maria Salud Pedros Cotino', 'Politico Gobierno');

INSERT INTO sc_vlci_userdb.public.granted_authorities (username,authority) 
    VALUES
        ('U813624','AccUnifEconCiudMoviAlca');

COMMIT;
