-- Deploy sc_vlci_userdb:v_migrations/TLF054-alta_usuario to pg
BEGIN;

INSERT INTO
    public.users (
        username,
        "password",
        enabled,
        description,
        "position"
    )
VALUES
    (
        'LMHM',
        'Password1',
        1,
        'Manolo Herrero',
        'Funcionario OCI'
    ),
    (
        'lmhm',
        'Password1',
        1,
        'Manolo Herrero',
        'Funcionario OCI'
    );

INSERT INTO
    public.granted_authorities (username, authority)
VALUES
    ('LMHM', 'funcionario_oci'),
    ('lmhm', 'funcionario_oci');

COMMIT;