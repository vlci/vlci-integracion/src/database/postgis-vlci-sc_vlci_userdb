-- Deploy sc_vlci_userdb:v_migrations/TLF037-cambio_rol to pg

BEGIN;

UPDATE public.granted_authorities
	SET "authority"='AccEconCiudMoviAlcaMedSen'
	WHERE username='UHEMUD';
COMMIT;
