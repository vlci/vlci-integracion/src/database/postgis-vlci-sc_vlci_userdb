-- Deploy sc_vlci_userdb:v_migrations/TLF036-alta_usuario to pg

BEGIN;

insert into public.users (username, password, enabled, description, position) 
	values ('UMOJEF', 'Password1', 1, 'Francisco Molleda Jerez', 'Idrica'),
    ('umojef', 'Password1', 1, 'Francisco Molleda Jerez', 'Idrica');

INSERT INTO sc_vlci_userdb.public.granted_authorities (username,authority) 
    VALUES
        ('UMOJEF','DBPentDeveloper'),
        ('umojef','DBPentDeveloper');

COMMIT;
