-- Deploy sc_vlci_userdb:v_migrations/TLF050-alta_baja_usuarios to pg

BEGIN;

DELETE FROM public.granted_authorities
	WHERE username in ('lrft','LRFT');

DELETE FROM public.users
	WHERE username in ('lrft','LRFT');

insert into public.users (username, password, enabled, description, position) 
	values ('U19915', 'Password1', 1, 'Manolo Herrero', 'Funcionario OCI'),
    ('u19915', 'Password1', 1, 'Manolo Herrero', 'Funcionario OCI');

INSERT INTO sc_vlci_userdb.public.granted_authorities (username,authority) 
    VALUES
        ('U19915','funcionario_oci'),
        ('u19915','funcionario_oci');


--Corrección nombres con espacio al inicio--
    UPDATE public.users
    SET
        "description" = 'Carles Salom'
    WHERE
        username in ('U813943', 'u813943');

    UPDATE public.users
    SET
        "description" = 'Josep Ribes'
    WHERE
        username in ('u813585', 'U813585');
COMMIT;
