-- Deploy sc_vlci_userdb:v_migrations/TLF052-agregar_usuarios_rol to pg
BEGIN;

INSERT INTO
    sc_vlci_userdb.public.authorities (authority, description)
VALUES
    (
        'funcionario_sertic',
        'Acceso a CdM Ciudad, Alcaldia, Geoportal y Sigval'
    ),
    (
        'jefe_servicio_sertic',
        'Acceso a CdM Ciudad, Alcaldia, Geoportal y Sigval'
    );

insert into public.users (username, password, enabled, description, position) 
	values ('LPBG', 'Password1', 1, 'Pedro Belenguer', 'Funcionario General'),
    ('lpbg', 'Password1', 1, 'Pedro Belenguer', 'Funcionario General'),
    ('LEBP', 'Password1', 1, 'Eloy Bonilla', 'Funcionario General'),
    ('lebp', 'Password1', 1, 'Eloy Bonilla', 'Funcionario General');

UPDATE public.granted_authorities
SET
    "authority" = 'jefe_servicio_sertic'
WHERE
    username in ('LVRI', 'lvri');

INSERT INTO sc_vlci_userdb.public.granted_authorities (username,authority) 
    VALUES
        ('LPBG','funcionario_sertic'),
        ('lpbg','funcionario_sertic'),
        ('LEBP','funcionario_sertic'),
        ('lebp','funcionario_sertic');

COMMIT;