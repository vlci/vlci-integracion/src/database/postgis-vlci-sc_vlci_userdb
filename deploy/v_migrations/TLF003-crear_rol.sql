-- Deploy sc_vlci_userdb:v_migrations/TLF003-crear_rol to pg

BEGIN;

insert into sc_vlci_userdb.public.authorities (authority, description) values ('DBPentDeveloperParticipacion', 'Desarrolladores area de participacion'), ('AccUnifEconCiudMoviAlca', 'Acceso a CdM Unificado, Economico, Ciudad, Movilidad y Alcaldia');

COMMIT;
