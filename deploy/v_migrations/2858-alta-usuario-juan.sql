-- Deploy sc_vlci_userdb:v_migrations/2858-alta-usuario-juan to pg

BEGIN;

insert into public.users (username, password, enabled, description, position) 
values ('ULULOJ', 'Password1', 1, 'Juan Lujan', 'Idrica'),
    ('ululoj', 'Password1', 1, 'Juan Lujan', 'Idrica');

insert into public.granted_authorities (username, authority)
values ('ULULOJ', 'DBPentDeveloper'),
    ('ululoj', 'DBPentDeveloper');


COMMIT;