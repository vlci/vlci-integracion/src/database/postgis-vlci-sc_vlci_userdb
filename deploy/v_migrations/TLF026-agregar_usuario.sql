-- Deploy sc_vlci_userdb:v_migrations/TLF026-agregar_usuario to pg

BEGIN;

insert into public.users (username, password, enabled, description, position) 
	values ('U16864', 'Password1', 1, 'Asuncion Abad Tora', 'Funcionario General'),
	    ('u16864', 'Password1', 1, 'Asuncion Abad Tora', 'Funcionario General');

INSERT INTO sc_vlci_userdb.public.granted_authorities (username,authority) 
    VALUES
        ('U16864','AccEconomico'),
        ('u16864','AccEconomico');


COMMIT;
