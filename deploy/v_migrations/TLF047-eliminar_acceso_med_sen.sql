-- Deploy sc_vlci_userdb:v_migrations/TLF047-eliminar_acceso_med_sen to pg
BEGIN;

UPDATE public.granted_authorities --AccEconCiudAlcaMedSen
SET
    "authority" = 'AccCiudadAlcaldia'
WHERE
    username in (
        'U18761',
        'u18761',
        'U301362',
        'u301362',
        'u301369',
        'U301369',
        'U813624',
        'u813624'
    );


INSERT INTO sc_vlci_userdb.public.authorities (authority, description) 
	VALUES 
		('AccCiudMoviAlca', 'Acceso a CdM Ciudad, Movilidad y Alcaldía');

UPDATE public.granted_authorities --AccEconCiudMoviAlcaMedSen
SET
    "authority" = 'AccCiudMoviAlca'
WHERE
    username in (
        'U813817',
        'U14585',
        'U16006',
        'U16655',
        'U17625',
        'U18805',
        'u813817',
        'u14585',
        'u16006',
        'u16655',
        'u17625',
        'u18805'
    );

COMMIT;