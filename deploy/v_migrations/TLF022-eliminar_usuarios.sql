-- Deploy sc_vlci_userdb:v_migrations/TLF022-eliminar_usuarios to pg

BEGIN;

DELETE FROM
	    public.granted_authorities
	WHERE username IN (
		'U18533', 'u18533',
		'U13513', 'u13513',
		'U16864', 'u16864',
		'USACEC', 'usacec',
		'U66220', 'u66220',
		'U19316', 'u19316',
		'U19488', 'u19488',
		'U14772', 'u14772',
		'U22529', 'u22529',
		'U19492', 'u19492',
		'U65159', 'u65159',
		'U17392', 'u17392',
		'U18336', 'u18336',
		'U18587', 'u18587',
		'U70096', 'u70096',
		'U16572', 'u16572',
		'U19946', 'u19946',
		'UYEFEJ', 'uyefej'
	);
	DELETE FROM
	    public.users
	WHERE username IN (
		'U18533', 'u18533',
		'U13513', 'u13513',
		'U16864', 'u16864',
		'USACEC', 'usacec',
		'U66220', 'u66220',
		'U19316', 'u19316',
		'U19488', 'u19488',
		'U14772', 'u14772',
		'U22529', 'u22529',
		'U19492', 'u19492',
		'U65159', 'u65159',
		'U17392', 'u17392',
		'U18336', 'u18336',
		'U18587', 'u18587',
		'U70096', 'u70096',
		'U16572', 'u16572',
		'U19946', 'u19946',
		'UYEFEJ', 'uyefej'
	);

COMMIT;
