-- Deploy sc_vlci_userdb:v_migrations/TLF015-crear_roles to pg

BEGIN;

INSERT INTO sc_vlci_userdb.public.authorities (authority, description) 
	VALUES 
		('AccEconomico', 'Acceso a CdM Economico'),
		('AccEconomicoMovilidad', 'Acceso a CdM Economico y Movilidad'),
		('AccMovilidad', 'Acceso a CdM Movilidad');
        
COMMIT;
