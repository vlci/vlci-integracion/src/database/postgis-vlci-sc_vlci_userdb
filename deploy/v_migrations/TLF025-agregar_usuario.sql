-- Deploy sc_vlci_userdb:v_migrations/TLF025-agregar_usuario to pg

BEGIN;

insert into public.users (username, password, enabled, description, position) 
	values ('U813817', 'Password1', 1, 'Jonatan Baena Lundgren', 'Funcionario General/Secretario'),
	    ('u813817', 'Password1', 1, 'Jonatan Baena Lundgren', 'Funcionario General/Secretario');

	INSERT INTO sc_vlci_userdb.public.granted_authorities (username,authority) VALUES
		('U813817','AccUnifEconCiudMoviAlca'),
	    ('u813817','AccUnifEconCiudMoviAlca');

COMMIT;
