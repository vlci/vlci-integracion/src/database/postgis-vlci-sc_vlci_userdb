-- Deploy sc_vlci_userdb:v_migrations/TLF010-eliminar_usuarios_BDO to pg
BEGIN;

DELETE FROM
    public.granted_authorities
WHERE
    username IN (
        'U301364',
        'u301364',
        'U301368',
        'u301368',
        'U75416',
        'u75416',
        'U301383',
        'u301383',
        'U74678',
        'u74678',
        'U75420',
        'u75420',
        'U18294',
        'u18294',
        'U301339',
        'u301339',
        'U301385',
        'u301385',
        'U301375',
        'u301375',
        'U301373',
        'u301373',
        'U75471',
        'u75471',
        'U18048',
        'u18048',
        'U17173',
        'u17173',
        'U301330',
        'u301330',
        'U301336',
        'u301336',
        'U301334',
        'u301334',
        'U10203',
        'u10203',
        'U301337',
        'u301337',
        'U301323',
        'u301323',
        'U301347',
        'u301347',
        'U50237',
        'u50237',
        'U301353',
        'u301353',
        'U19236',
        'u19236',
        'U301381',
        'u301381',
        'U18758',
        'u18758',
        'U47055',
        'u47055',
        'UGUAGJ',
        'uguagj',
        'U301357',
        'u301357',
        'U301367',
        'u301367',
        'U75472',
        'u75472',
        'U18353',
        'u18353',
        'U19017',
        'u19017',
        'U18095',
        'u18095',
        'U301374',
        'u301374',
        'U19028',
        'u19028',
        'U301366',
        'u301366',
        'U301335',
        'u301335',
        'U301363',
        'u301363',
        'U301382',
        'u301382',
        'U301278',
        'u301278',
        'U60361',
        'u60361',
        'U18994',
        'u18994',
        'U301350',
        'u301350',
        'U301338',
        'u301338',
        'U65692',
        'u65692',
        'U18056',
        'u18056',
        'U19535',
        'u19535',
        'U301391',
        'u301391'
    );

DELETE FROM
    public.users
WHERE
    username IN (
        'U301364',
        'u301364',
        'U301368',
        'u301368',
        'U75416',
        'u75416',
        'U301383',
        'u301383',
        'U74678',
        'u74678',
        'U75420',
        'u75420',
        'U18294',
        'u18294',
        'U301339',
        'u301339',
        'U301385',
        'u301385',
        'U301375',
        'u301375',
        'U301373',
        'u301373',
        'U75471',
        'u75471',
        'U18048',
        'u18048',
        'U17173',
        'u17173',
        'U301330',
        'u301330',
        'U301336',
        'u301336',
        'U301334',
        'u301334',
        'U10203',
        'u10203',
        'U301337',
        'u301337',
        'U301323',
        'u301323',
        'U301347',
        'u301347',
        'U50237',
        'u50237',
        'U301353',
        'u301353',
        'U19236',
        'u19236',
        'U301381',
        'u301381',
        'U18758',
        'u18758',
        'U47055',
        'u47055',
        'UGUAGJ',
        'uguagj',
        'U301357',
        'u301357',
        'U301367',
        'u301367',
        'U75472',
        'u75472',
        'U18353',
        'u18353',
        'U19017',
        'u19017',
        'U18095',
        'u18095',
        'U301374',
        'u301374',
        'U19028',
        'u19028',
        'U301366',
        'u301366',
        'U301335',
        'u301335',
        'U301363',
        'u301363',
        'U301382',
        'u301382',
        'U301278',
        'u301278',
        'U60361',
        'u60361',
        'U18994',
        'u18994',
        'U301350',
        'u301350',
        'U301338',
        'u301338',
        'U65692',
        'u65692',
        'U18056',
        'u18056',
        'U19535',
        'u19535',
        'U301391',
        'u301391'
    );

COMMIT;