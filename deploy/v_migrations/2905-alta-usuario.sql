-- Deploy sc_vlci_userdb:v_migrations/2905-alta-usuario to pg

BEGIN;

INSERT INTO public.users (username, password, enabled, description, position) 
	VALUES ('U301369', 'Password1', 1, 'María José Catalá Verdet', 'Alcaldesa');
INSERT INTO public.users (username, password, enabled, description, position) 
	VALUES ('u301369', 'Password1', 1, 'María José Catalá Verdet', 'Alcaldesa');

INSERT INTO sc_vlci_userdb.public.granted_authorities (username,authority) 
    VALUES
        ('U301369','AccEconCiudAlcaMedSen');
INSERT INTO sc_vlci_userdb.public.granted_authorities (username,authority) 
    VALUES
        ('u301369','AccEconCiudAlcaMedSen');

COMMIT;
