-- Deploy sc_vlci_userdb:v_migrations/TLF053-cambio_permisos_rol to pg

BEGIN;

ALTER TABLE public.authorities ALTER COLUMN description TYPE varchar(200) USING description::varchar(200);

UPDATE sc_vlci_userdb.public.authorities
SET description = 'Acceso a CdM Ciudad, Alcaldia, Movilidad, Geoportal, Sigval, SCC, Mapa Poblacion, Incidencias, Incidencias Detalle y Estadisticas DANA'
WHERE authority = 'politico_gobierno';

UPDATE sc_vlci_userdb.public.authorities
SET description = 'Acceso a CdM Ciudad, Alcaldia, Economico, Movilidad, Movilidad Antiguo, Geoportal, Sigval, SCC, Mapa Poblacion, Incidencias, Incidencias Detalle y Estadisticas DANA'
WHERE authority = 'director_gestion_datos';

UPDATE sc_vlci_userdb.public.authorities
SET description = 'Acceso a CdM Ciudad, Alcaldia, Movilidad, Geoportal, Sigval, SCC, Mapa Poblacion, Incidencias, Incidencias Detalle y Estadisticas DANA'
WHERE authority = 'secretario';

UPDATE sc_vlci_userdb.public.authorities
SET description = 'Acceso a CdM Ciudad, Movilidad, Movilidad Antiguo, Geoportal y Sigval'
WHERE authority = 'jefe_servicio_movilidad';

UPDATE sc_vlci_userdb.public.authorities
SET description = 'Acceso a CdM Ciudad, Alcaldia, Geoportal, Sigval, Mapa Poblacion, Incidencias, Incidencias Detalle y Estadisticas DANA'
WHERE authority in ('jefe_servicio_sertic','funcionario_sertic');

UPDATE sc_vlci_userdb.public.authorities
SET description = 'Acceso a CdM Ciudad, Movilidad, Movilidad Antiguo, Geoportal y Sigval'
WHERE authority = 'funcionario_movilidad';

UPDATE sc_vlci_userdb.public.authorities
SET description = 'Acceso a CdM Ciudad, Alcaldia, Movilidad, Geoportal, Sigval, SCC, Medioambiente, Sensores, Mapa Poblacion, Incidencias, Incidencias Detalle y Estadisticas DANA'
WHERE authority = 'funcionario_oci';

UPDATE sc_vlci_userdb.public.authorities
SET description = 'Acceso a CdM Ciudad, Alcaldia, Movilidad, Geoportal, Sigval, SCC, Medioambiente, Sensores, Wifi, Mapa Poblacion, Incidencias, Incidencias Detalle y Estadisticas DANA'
WHERE authority = 'funcionario_oci_wifi';

COMMIT;