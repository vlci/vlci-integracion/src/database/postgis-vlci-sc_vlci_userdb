-- Deploy sc_vlci_userdb:v_migrations/TLF048-alta_usuario to pg

BEGIN;

insert into public.users (username, password, enabled, description, position) 
	values ('ULIMAM', 'Password1', 1, 'Miguel Angel Llinares', 'Idrica'),
    ('ulimam', 'Password1', 1, 'Miguel Angel Llinares', 'Idrica');

INSERT INTO sc_vlci_userdb.public.granted_authorities (username,authority) 
    VALUES
        ('ULIMAM','DBPentDeveloper'),
        ('ulimam','DBPentDeveloper');

COMMIT;
