-- Deploy sc_vlci_userdb:v_migrations/TLF023-agregar_usuarios to pg

BEGIN;

	insert into public.users (username, password, enabled, description, position) 
	values ('U19535', 'Password1', 1, 'Vicente Rodrigo Ingresa', 'Jefe de Servicio'),
	    ('u19535', 'Password1', 1, 'Vicente Rodrigo Ingresa', 'Jefe de Servicio');

	INSERT INTO sc_vlci_userdb.public.granted_authorities (username,authority) VALUES
		('U19535','AccEconomico'),
	    ('u19535','AccEconomico');


COMMIT;
