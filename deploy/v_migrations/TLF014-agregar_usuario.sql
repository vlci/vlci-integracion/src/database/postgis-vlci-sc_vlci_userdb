-- Deploy sc_vlci_userdb:v_migrations/TLF014-agregar_usuario to pg

BEGIN;

insert into public.users (username, password, enabled, description, position) 
values ('U301335', 'Password1', 1, 'Pere Fuset i Tortosa', 'Politico Oposicion'),
    ('u301335', 'Password1', 1, 'Pere Fuset i Tortosa', 'Politico Oposicion');

INSERT INTO sc_vlci_userdb.public.granted_authorities (username,authority) VALUES
	('U301335','AccCiudad'),
    ('u301335','AccCiudad');
    
COMMIT;
