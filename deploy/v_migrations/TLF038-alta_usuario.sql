-- Deploy sc_vlci_userdb:v_migrations/TLF038-alta_usuario to pg

BEGIN;

insert into public.users (username, password, enabled, description, position) 
	values ('UORICJ', 'Password1', 1, 'Jose Ignacio Ortola', 'Idrica'),
    ('uoricj', 'Password1', 1, 'Jose Ignacio Ortola', 'Idrica'),
    ('UCDM1', 'Password1', 1, 'Cuadros de Mando', 'Sistema/Alcaldia'),
    ('ucdm1', 'Password1', 1, 'Cuadros de Mando', 'Sistema/Alcaldia');

INSERT INTO sc_vlci_userdb.public.granted_authorities (username,authority) 
    VALUES
        ('UORICJ','DBPentDeveloper'),
        ('uoricj','DBPentDeveloper'),
        ('UCDM1','AccEconCiudAlcaMedSen'),
        ('ucdm1','AccEconCiudAlcaMedSen');

UPDATE public.users
	SET "position"='Politico Gobierno'
	WHERE username in ('U301369','u301369');        

COMMIT;
