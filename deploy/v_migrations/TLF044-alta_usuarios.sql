-- Deploy sc_vlci_userdb:v_migrations/TLF044-alta_usuarios to pg

BEGIN;

insert into public.users (username, password, enabled, description, position) 
	values ('U813585', 'Password1', 1, ' Josep Ribes', 'Funcionario General/Director'),
    ('u813585', 'Password1', 1, ' Josep Ribes', 'Funcionario General/Director'),
    ('U813943', 'Password1', 1, ' Carles Salom', 'Funcionario General/Director'),
    ('u813943', 'Password1', 1, ' Carles Salom', 'Funcionario General/Director');

INSERT INTO sc_vlci_userdb.public.granted_authorities (username,authority) 
    VALUES
        ('U813585','AccEconCiudAlcaMedSen'),
        ('u813585','AccEconCiudAlcaMedSen'),
        ('U813943','AccEconCiudAlcaMedSen'),
        ('u813943','AccEconCiudAlcaMedSen');

COMMIT;
