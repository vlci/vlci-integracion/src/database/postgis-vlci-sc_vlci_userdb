-- Deploy sc_vlci_userdb:v_migrations/TLF046-cambiar_rol_usuario to pg

BEGIN;

INSERT INTO sc_vlci_userdb.public.authorities (authority, description) 
	VALUES 
		('AccEconCiudMoviAlca', 'Acceso a CdM Enonómico, Ciudad, Movilidad y Alcaldía');


UPDATE public.granted_authorities
	SET "authority"='AccEconCiudMoviAlca'
	WHERE username in (
        'U813585',
        'u813585',
        'U813943',
        'u813943'
    );

COMMIT;
