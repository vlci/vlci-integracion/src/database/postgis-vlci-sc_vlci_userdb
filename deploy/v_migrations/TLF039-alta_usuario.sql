-- Deploy sc_vlci_userdb:v_migrations/TLF039-alta_usuario to pg

BEGIN;

insert into public.users (username, password, enabled, description, position) 
	values ('U811919', 'Password1', 1, 'Inmaculada Blanco', 'Funcionario General'),
    ('u811919', 'Password1', 1, 'Inmaculada Blanco', 'Funcionario General');

INSERT INTO sc_vlci_userdb.public.granted_authorities (username,authority) 
    VALUES
        ('U811919','AccEconCiudMoviAlcaMedSen'),
        ('u811919','AccEconCiudMoviAlcaMedSen');


COMMIT;
