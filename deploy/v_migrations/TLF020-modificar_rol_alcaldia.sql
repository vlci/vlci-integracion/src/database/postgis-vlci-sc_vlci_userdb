-- Deploy sc_vlci_userdb:v_migrations/TLF020-modificar_rol_alcaldia to pg

BEGIN;

	UPDATE sc_vlci_userdb.public.granted_authorities
	SET authority='AccUnifEconCiudMoviAlca' 
	WHERE username IN (
	    'LEFC', 'lefc',
	    'LRFT', 'lrft',
	    'U16655', 'u16655',
	    'U17625', 'u17625',
	    'U16006', 'u16006',
	    'U18805', 'u18805',
	    'U14585', 'u14585'
	);


COMMIT;
