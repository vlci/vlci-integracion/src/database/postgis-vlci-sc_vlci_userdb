-- Deploy sc_vlci_userdb:v_migrations/TLF033-eliminar-acceso-unificado to pg
BEGIN;

--Crear rol sin Unificado
INSERT INTO
    sc_vlci_userdb.public.authorities (authority, description)
VALUES
    (
        'AccEconCiudMoviAlca',
        'Acceso a CdM Economico, Ciudad, Movilidad y Alcaldia'
    );

--Cambiar rol
UPDATE
    sc_vlci_userdb.public.granted_authorities
SET
    authority = 'AccEconCiudMoviAlca'
WHERE
    username IN (
        'LEFC',
        'lefc',
        'LJCG',
        'LJPL',
        'LRFT',
        'U13580',
        'U14585',
        'U16006',
        'U16655',
        'U16656',
        'U17569',
        'U17625',
        'U18133',
        'U18761',
        'U18805',
        'U301362',
        'U813624',
        'U813817',
        'ljcg',
        'ljpl',
        'lrft',
        'u13580',
        'u14585',
        'u16006',
        'u16655',
        'u16656',
        'u17569',
        'u17625',
        'u18133',
        'u18761',
        'u18805',
        'u301362',
        'u813624',
        'u813817',
        'ubamod'
    );

--Eliminar roles
delete from
    sc_vlci_userdb.public.authorities
where
    authority IN (
        'AccUnificado',
        'AccUnificadoEconomico',
        'AccUnificadoEconomicoCiudad',
        'AccUnificadoEconomicoCiudadMovilidad',
        'AccUnificadoEconomicoMovilidad',
        'AccUnificadoMovilidad',
        'AccUnifEconCiudMoviAlca'
    );

COMMIT;