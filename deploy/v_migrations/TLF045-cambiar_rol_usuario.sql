-- Deploy sc_vlci_userdb:v_migrations/TLF045-cambiar_rol_usuario to pg

BEGIN;

INSERT INTO sc_vlci_userdb.public.authorities (authority, description) 
	VALUES 
		('AccCiudadAlcaldia', 'Acceso a CdM Ciudad y Alcaldía');


UPDATE public.granted_authorities
	SET "authority"='AccCiudadAlcaldia'
	WHERE username in (
        'UCDM1','ucdm1'
    );

COMMIT;
