-- Deploy sc_vlci_userdb:v_migrations/TLF001-eliminar_usuarios_duplicados to pg

BEGIN;

DELETE FROM public.granted_authorities
WHERE ctid NOT IN (
  SELECT DISTINCT ON (username, authority) ctid
  FROM public.granted_authorities
);

COMMIT;
