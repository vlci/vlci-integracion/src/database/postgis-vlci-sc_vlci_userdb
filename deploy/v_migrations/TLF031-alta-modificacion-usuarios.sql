-- Deploy sc_vlci_userdb:v_migrations/TLF031-alta-modificacion-usuarios to pg

BEGIN;

/*Crear rol*/
INSERT INTO sc_vlci_userdb.public.authorities (authority, description) 
	VALUES 
		('AccCiudadMovilidad', 'Acceso a CdM Ciudad y Movilidad');

/*Modificar roles*/
UPDATE public.granted_authorities
	SET "authority"='AccEconomicoMovilidadCiudad'
	WHERE username= 'u19895';

UPDATE public.granted_authorities
	SET "authority"='AccCiudadMovilidad'
	WHERE username in ('U66173','u66173', 'U17994','u17994');

/*Crear usuarios*/
insert into public.users (username, password, enabled, description, position) 
	values ('U67016', 'Password1', 1, 'Jesus Caballero', 'Funcionario General'),
	    ('u67016', 'Password1', 1, 'Jesus Caballero', 'Funcionario General'),
        ('U18218', 'Password1', 1, 'Maria Carmen Monreal', 'Funcionario General'),
	    ('u18218', 'Password1', 1, 'Maria Carmen Monreal', 'Funcionario General');

INSERT INTO sc_vlci_userdb.public.granted_authorities (username,authority) VALUES
    ('U67016','AccCiudadMovilidad'),
    ('u67016','AccCiudadMovilidad'),
    ('U18218','AccCiudadMovilidad'),
    ('u18218','AccCiudadMovilidad');

COMMIT;
