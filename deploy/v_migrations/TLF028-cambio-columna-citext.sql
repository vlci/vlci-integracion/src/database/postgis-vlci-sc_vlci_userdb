-- Deploy sc_vlci_userdb:v_migrations/TLF028-cambio-columna-citext to pg

BEGIN;

-- Eliminar usuarios duplicados de users
DELETE FROM public.granted_authorities
WHERE LOWER(username) = username;

DELETE FROM public.users
WHERE LOWER(username) = username;

ALTER TABLE sc_vlci_userdb.public.granted_authorities ALTER COLUMN username TYPE public.citext;

ALTER TABLE sc_vlci_userdb.public.users 
ALTER COLUMN username TYPE public.citext;

COMMIT;
