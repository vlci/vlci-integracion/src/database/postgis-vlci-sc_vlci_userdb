-- Deploy sc_vlci_userdb:v_migrations/TLF049-nuevos_roles to pg
BEGIN;

-------------------- Eliminar Usuarios -----------------------
    DELETE FROM public.granted_authorities
    WHERE
        username in ('UALFAF', 'ualfaf', 'U813647', 'u813647');

    DELETE FROM public.users
    WHERE
        username in ('UALFAF', 'ualfaf', 'U813647', 'u813647');

------------------- Cambio Cargo y Nombres ---------------------
    UPDATE public.users
    SET
        "description" = 'Marc Correcher Rigau'
    WHERE
        username in ('U16996', 'u16996');

    UPDATE public.users
    SET
        "description" = 'Maria Jose Magraner Llinares'
    WHERE
        username in ('U19896', 'u19896');

    UPDATE public.users
    SET
        "description" = 'Antonio Molla Calabuig'
    WHERE
    username in ('U50496', 'u50496');

    UPDATE public.users
    SET
        "position" = 'Jefe de servicio'
    WHERE
        username in (
            'U16608',
            'u16608',
            'U19537',
            'u19537',
            'U19896',
            'u19896'
        );

---------------------------Crear roles--------------------------
    INSERT INTO
        sc_vlci_userdb.public.authorities (authority, description)
    VALUES      
        (
            'funcionario_movilidad',
            'Acceso a CdM Ciudad, Movilidad, Geoportal y Sigval'
        ),
        (
            'director_gestion_datos',
            'Acceso a CdM Ciudad, Alcaldia, Economico, Movilidad, Geoportal, Sigval y SCC'
        ),
        (
            'funcionario_oci',
            'Acceso a CdM Ciudad, Alcaldia, Geoportal, Sigval, SCC, Medioambiente y Sensores'
        ),
        (
            'funcionario_oci_wifi',
            'Acceso a CdM Ciudad, Alcaldia, Geoportal, Sigval, SCC, Medioambiente, Sensores y Wifi'
        ),
        ('jefe_servicio', 'Acceso a Geoportal y Sigval'),
        (
            'jefe_servicio_medioambiente',
            'Acceso a CdM Ciudad, Medioambiente, Geoportal y Sigval'
        ),
        (
            'jefe_servicio_movilidad',
            'Acceso a CdM Ciudad, Geoportal y Sigval'
        ),
        (
            'politico_gobierno',
            'Acceso a CdM Ciudad, Alcaldia, Geoportal, Sigval y SCC'
        ),
        ('politico_oposicion', 'Acceso a CdM Ciudad'),
        (
            'secretario',
            'Acceso a CdM Ciudad, Alcaldia, Geoportal, Sigval y SCC'
        );

-------------------------DBPentDeveloper------------------------
    UPDATE public.granted_authorities
    SET
        "authority" = 'DBPentDeveloper'
    WHERE
        username in ('UHEMUD', 'ubamod');

----------------------Director Gestion Datos--------------------
    UPDATE public.granted_authorities
    SET
        "authority" = 'director_gestion_datos'
    WHERE
        username in ('U813585', 'u813585', 'U813943', 'u813943');

-------------------------Funcionario Movilidad-------------------------
    UPDATE public.granted_authorities
    SET
        "authority" = 'funcionario_movilidad'
    WHERE
        username in (
            'U17994',
            'u17994',
            'U66173',
            'u66173',
            'U67016',
            'u67016',
            'U18218',
            'u18218',
            'U813012',
            'u813012'
        );

----------------------------Funcionario OCI----------------------------
    UPDATE public.granted_authorities
    SET
        "authority" = 'funcionario_oci'
    WHERE
        username in (
            'LEFC',
            'lefc',
            'LJPL',
            'ljpl',
            'LRFT',
            'lrft',
            'u13580',
            'U13580',
            'U17569',
            'u17569',
            'U18133',
            'u18133'
        );

--------------------------Funcionario OCI Wifi-------------------------
    UPDATE public.granted_authorities
    SET
        "authority" = 'funcionario_oci_wifi'
    WHERE
        username in ('LJCG', 'ljcg', 'U16656', 'u16656');

---------------------------Jefes de Servicio---------------------------
    UPDATE public.granted_authorities
    SET
        "authority" = 'jefe_servicio'
    WHERE
        username in (
            'U16864',
            'u16864',
            'U10807',
            'u10807',
            'U11020',
            'u11020',
            'U16675',
            'u16675',
            'U16678',
            'u16678',
            'U16742',
            'u16742',
            'U16852',
            'u16852',
            'U17389',
            'u17389',
            'U17390',
            'u17390',
            'U17394',
            'u17394',
            'U17398',
            'u17398',
            'U18330',
            'u18330',
            'U19261',
            'u19261',
            'U19441',
            'u19441',
            'U22346',
            'u22346',
            'U22348',
            'u22348',
            'U39892',
            'u39892',
            'U50483',
            'u50483',
            'U17381',
            'u17381',
            'LVRI',
            'lvri',
            'U16608',
            'u16608',
            'U16683',
            'u16683',
            'U16733',
            'u16733',
            'U16863',
            'u16863',
            'U17387',
            'u17387',
            'U17412',
            'u17412',
            'U18304',
            'u18304',
            'U19109',
            'u19109',
            'U19436',
            'u19436',
            'U19479',
            'u19479',
            'U19897',
            'u19897',
            'U47100',
            'u47100',
            'U60051',
            'u60051',
            'USAFEM',
            'usafem',
            'LMAG',
            'lmag',
            'U16016',
            'u16016',
            'U17040',
            'u17040',
            'U18041',
            'u18041',
            'U18427',
            'u18427',
            'U18868',
            'u18868',
            'U19233',
            'u19233',
            'U19370',
            'u19370',
            'U19437',
            'u19437',
            'U19537',
            'u19537',
            'U29661',
            'u29661',
            'U65852',
            'u65852',
            'U10923',
            'u10923',
            'U11007',
            'u11007',
            'U16671',
            'u16671',
            'U16866',
            'u16866',
            'U16928',
            'u16928',
            'U16996',
            'u16996',
            'U17068',
            'u17068',
            'U17963',
            'u17963',
            'U18542',
            'u18542',
            'U19439',
            'u19439',
            'U39078',
            'u39078',
            'U10809',
            'u10809',
            'U16007',
            'u16007',
            'U17385',
            'u17385',
            'U18050',
            'u18050',
            'U18520',
            'u18520',
            'U19442',
            'u19442',
            'U19896',
            'u19896',
            'U21571',
            'u21571',
            'U50496',
            'u50496'
        );

--------------------Jefes de Servicio Medioambiente--------------------
    UPDATE public.granted_authorities
    SET
        "authority" = 'jefe_servicio_medioambiente'
    WHERE
        username in ('U17399', 'u17399');

----------------------Jefes de Servicio Movilidad----------------------
    UPDATE public.granted_authorities
    SET
        "authority" = 'jefe_servicio_movilidad'
    WHERE
        username in ('U19895', 'u19895');

---------------------------Politico Gobierno---------------------------
    UPDATE public.granted_authorities
    SET
        "authority" = 'politico_gobierno'
    WHERE
        username in (
            'UCDM1',
            'ucdm1',
            'U18761',
            'u18761',
            'U301362',
            'u301362',
            'U301369',
            'u301369',
            'U813624',
            'u813624'
        );

--------------------------Politico Oposicion---------------------------
    UPDATE public.granted_authorities
    SET
        "authority" = 'politico_oposicion'
    WHERE
        username in ('U301335', 'u301335', 'uhemud');

------------------------------Secretario-------------------------------
    UPDATE public.granted_authorities
    SET
        "authority" = 'secretario'
    WHERE
        username in ('U14585', 'u14585',
        'U16006', 'u16006',
        'U16655', 'u16655',
        'U17625', 'u17625',
        'U18805', 'u18805',
        'U813817', 'u813817');

----------------------------Eliminar roles-----------------------------
    delete from sc_vlci_userdb.public.authorities
    where
        authority in (
            'AccEconCiudAlcaMedSen',
            'AccEconCiudMoviAlcaMedSen',
            'AccEconCiudMoviAlcaMedSenWif',
            'AccCiudMoviAlca'
            'AccEconCiudMoviAlca',
            'AccCiudadMovilidad',
            'AccMovilidad',
            'AccEconomico',
            'AccEconMed',
            'AccEconomicoMovilidadCiudad',
            'AccCiudad',
            'AccCiudadAlcaldia',
            'AccEconomicoMovilidad',
            'AccEconCiudMoviAlca',
            'AccCiudMoviAlca'
        );
COMMIT;