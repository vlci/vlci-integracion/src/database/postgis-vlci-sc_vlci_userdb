-- Deploy sc_vlci_userdb:v_migrations/TLF051-cambio_rol to pg
BEGIN;

UPDATE public.granted_authorities
SET
    "authority" = 'politico_gobierno'
WHERE
    username in ('uhemud', 'ubamod');

COMMIT;