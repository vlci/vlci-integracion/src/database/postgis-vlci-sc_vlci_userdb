-- Deploy sc_vlci_userdb:v_migrations/2905-cambiar-rol-usuarios to pg

BEGIN;

UPDATE sc_vlci_userdb.public.granted_authorities
SET authority='AccEconCiudAlcaMedSen' where username in('U18761','u18761','U301362','u301362');

COMMIT;
