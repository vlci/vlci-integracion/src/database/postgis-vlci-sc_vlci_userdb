-- Deploy sc_vlci_userdb:v_migrations/TLF005-agregar_usuarios to pg

BEGIN;

insert into public.users (username, password, enabled, description) 
values ('UBAMOD', 'Password1', 1, 'David Barroso Moro'),
    ('ubamod', 'Password1', 1, 'David Barroso Moro'),
    ('U18133', 'Password1', 1, 'José Pinedo Luján'),
    ('u18133', 'Password1', 1, 'José Pinedo Luján'),
    ('U16656', 'Password1', 1, 'Juan Lucas Calderón García'),
    ('u16656', 'Password1', 1, 'Juan Lucas Calderón García'),
    ('U17569', 'Password1', 1, 'Ana Lacruz Miralles'),
    ('u17569', 'Password1', 1, 'Ana Lacruz Miralles'),
    ('UGUAGJ', 'Password1', 1, 'Juan Ramón Gutiérrez Agullo'),
    ('uguagj', 'Password1', 1, 'Juan Ramón Gutiérrez Agullo');

INSERT INTO sc_vlci_userdb.public.granted_authorities (username,authority) VALUES
	('UBAMOD','DBPentDeveloper'),
    ('ubamod','DBPentDeveloper'),
    ('U18133','AccUnifEconCiudMoviAlca'),
    ('u18133','AccUnifEconCiudMoviAlca'),
    ('U16656','AccUnifEconCiudMoviAlca'),
    ('u16656','AccUnifEconCiudMoviAlca'),
    ('U17569','AccUnifEconCiudMoviAlca'),
    ('u17569','AccUnifEconCiudMoviAlca'),
    ('UGUAGJ','DBPentDeveloper'),
    ('uguagj','DBPentDeveloper');

COMMIT;
