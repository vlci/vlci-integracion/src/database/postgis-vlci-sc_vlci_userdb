-- Deploy sc_vlci_userdb:v_migrations/TLF034-cambios_portada to pg
BEGIN;

--Crear nuevos roles
INSERT INTO
    sc_vlci_userdb.public.authorities (authority, description)
VALUES
    (
        'AccEconCiudMoviAlcaMedSen',
        'Acceso a CdM Economico, Ciudad, Movilidad, Alcaldia, Medioambiente y Sensores'
    ),
    (
        'AccEconCiudMoviAlcaMedSenWif',
        'Acceso a CdM Economico, Ciudad, Movilidad, Alcaldia, Medioambiente, Sensores y Wifi'
    ),
    (
        'AccEconMed',
        'Acceso a CdM Economico y Medioambiente'
    );

--Asignar nuevos roles
UPDATE
    sc_vlci_userdb.public.granted_authorities
SET
    authority = 'AccEconCiudMoviAlcaMedSen'
WHERE
    authority = 'AccEconCiudMoviAlca';

UPDATE
    sc_vlci_userdb.public.granted_authorities
SET
    authority = 'AccEconCiudMoviAlcaMedSenWif'
WHERE
    username IN ('LJCG', 'ljcg', 'U16656', 'u16656');

UPDATE
    sc_vlci_userdb.public.granted_authorities
SET
    authority = 'AccEconMed'
WHERE
    username IN ('U17399', 'u17399');

--Eliminar roles deprecados
delete from
    sc_vlci_userdb.public.authorities
where
    authority = 'AccEconCiudMoviAlca';

COMMIT;