-- Deploy sc_vlci_userdb:v_migrations/TLF043-eliminar_usuarios_deprecados to pg
BEGIN;

DELETE FROM public.granted_authorities
WHERE
    username in (
        'U10917',
        'U16516',
        'U16680',
        'U18175',
        'U18259',
        'U18452',
        'U19898',
        'U39098',
        'U60334',
        'U60350',
        'U65944',
        'U18541',
        'U65701',
        'U19535',
        'u10917',
        'u16516',
        'u16680',
        'u18175',
        'u18259',
        'u18452',
        'u19898',
        'u39098',
        'u60334',
        'u60350',
        'u65944',
        'u18541',
        'u65701',
        'u19535'
    );

DELETE FROM public.users
WHERE
    username in (
        'U10917',
        'U16516',
        'U16680',
        'U18175',
        'U18259',
        'U18452',
        'U19898',
        'U39098',
        'U60334',
        'U60350',
        'U65944',
        'U18541',
        'U65701',
        'U19535',
        'u10917',
        'u16516',
        'u16680',
        'u18175',
        'u18259',
        'u18452',
        'u19898',
        'u39098',
        'u60334',
        'u60350',
        'u65944',
        'u18541',
        'u65701',
        'u19535'
    );

COMMIT;