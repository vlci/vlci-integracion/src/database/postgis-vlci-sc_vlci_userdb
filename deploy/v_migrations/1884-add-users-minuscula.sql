-- Deploy sc_vlci_userdb:v_migrations/1884-add-users-minuscula to pg

BEGIN;

insert into public.users (username, password, enabled, description) 
values ('u18761', 'Password1', 1, 'Sonsoles Catala Verdet'),
    ('u301362', 'Password1', 1, 'Paula María Llovert Vilarrasa');

insert into public.granted_authorities (username, authority)
values ('u18761', 'AccUnificadoEconomicoCiudadMovilidad'),
    ('u301362', 'AccUnificadoEconomicoCiudadMovilidad');

COMMIT;

