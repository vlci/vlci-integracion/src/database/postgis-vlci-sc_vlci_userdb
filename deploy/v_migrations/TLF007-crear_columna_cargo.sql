-- Deploy sc_vlci_userdb:v_migrations/TLF007-crear_columna_cargo to pg

BEGIN;

ALTER TABLE sc_vlci_userdb.public.users ADD "position" varchar(100) NULL;

COMMIT;
