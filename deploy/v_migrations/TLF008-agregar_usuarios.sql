-- Deploy sc_vlci_userdb:v_migrations/TLF008-agregar_usuarios to pg

BEGIN;

insert into public.users (username, password, enabled, description) 
values ('URUSAS', 'Password1', 1, 'Santiago Ruiz Sánchez'),
    ('urusas', 'Password1', 1, 'Santiago Ruiz Sánchez'),
    ('LJPL', 'Password1', 1, 'José Pinedo Luján'),
    ('ljpl', 'Password1', 1, 'José Pinedo Luján');

INSERT INTO sc_vlci_userdb.public.granted_authorities (username,authority) VALUES
	('URUSAS','DBPentDeveloperParticipacion'),
    ('urusas','DBPentDeveloperParticipacion'),
    ('LJPL','AccUnifEconCiudMoviAlca'),
    ('ljpl','AccUnifEconCiudMoviAlca');

COMMIT;
