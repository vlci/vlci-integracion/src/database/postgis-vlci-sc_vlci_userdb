-- Deploy sc_vlci_userdb:v_migrations/TLF042-cambiar_rol_usuario to pg

BEGIN;

UPDATE public.granted_authorities
	SET "authority"='DBPentDeveloper'
	WHERE username in (
        'U811919','u811919'
    );

UPDATE public.users
	SET "position"='Funcionario OCI'
	WHERE username in (
        'U811919','u811919'
    );

COMMIT;
