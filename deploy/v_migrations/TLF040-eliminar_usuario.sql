-- Deploy sc_vlci_userdb:v_migrations/TLF040-eliminar_usuario to pg

BEGIN;

DELETE FROM public.granted_authorities
	WHERE username in ('UMAMAS','umamas');

DELETE FROM public.users
	WHERE username in ('UMAMAS','umamas');
COMMIT;
