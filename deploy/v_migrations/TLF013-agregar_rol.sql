-- Deploy sc_vlci_userdb:v_migrations/TLF013-agregar_rol to pg

BEGIN;

insert into sc_vlci_userdb.public.authorities (authority, description) values ('AccCiudad', 'Acceso a CdM Ciudad');

COMMIT;
