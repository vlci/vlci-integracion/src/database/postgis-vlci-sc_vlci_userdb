-- Deploy sc_vlci_userdb:v_migrations/1884-add-users to pg

BEGIN;

insert into public.users (username, password, enabled, description) 
values ('U18761', 'Password1', 1, 'Sonsoles Catala Verdet'),
    ('U301362', 'Password1', 1, 'Paula María Llovert Vilarrasa');

insert into public.granted_authorities (username, authority)
values ('U18761', 'AccUnificadoEconomicoCiudadMovilidad'),
    ('U301362', 'AccUnificadoEconomicoCiudadMovilidad');

COMMIT;
