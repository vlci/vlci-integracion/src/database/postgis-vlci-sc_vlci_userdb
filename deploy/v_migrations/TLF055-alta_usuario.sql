-- Deploy sc_vlci_userdb:v_migrations/TLF055-alta_usuario to pg

BEGIN;

INSERT INTO
    public.users (
        username,
        "password",
        enabled,
        description,
        "position"
    )
VALUES
    (
        'UPICEI',
        'Password1',
        1,
        'Ivan Piqueras',
        'Idrica'
    ),
    (
        'upicei',
        'Password1',
        1,
        'Ivan Piqueras',
        'Idrica'
    );

INSERT INTO
    public.granted_authorities (username, authority)
VALUES
    ('UPICEI', 'DBPentDeveloper'),
    ('upicei', 'DBPentDeveloper');

COMMIT;
