-- Deploy sc_vlci_userdb:v_migrations/2905-nuevo-rol to pg

BEGIN;

insert into sc_vlci_userdb.public.authorities (authority, description) values ('AccEconCiudAlcaMedSen' , 'Acceso a CdM Economico, Ciudad, Alcaldia, Medioambiente y Sensores');

COMMIT;
