-- Deploy sc_vlci_userdb:v_migrations/TLF041-cambiar_rol_usuario to pg

BEGIN;

UPDATE public.granted_authorities
	SET "authority"='AccEconCiudAlcaMedSen'
	WHERE username in (
        'U813624','u813624'
    );

COMMIT;
