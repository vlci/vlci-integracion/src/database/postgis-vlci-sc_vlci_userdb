-- Revert sc_vlci_userdb:v_migrations/TLF045-cambiar_rol_usuario from pg

BEGIN;

UPDATE public.granted_authorities
	SET "authority"='AccEconCiudAlcaMedSen'
	WHERE username in (
        'UCDM1','ucdm1'
    );

delete from sc_vlci_userdb.public.authorities where authority ='AccCiudadAlcaldia';


COMMIT;
