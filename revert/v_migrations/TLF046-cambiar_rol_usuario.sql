-- Revert sc_vlci_userdb:v_migrations/TLF046-cambiar_rol_usuario from pg

BEGIN;

UPDATE public.granted_authorities
	SET "authority"='AccEconCiudAlcaMedSen'
	WHERE username in (
        'U813585',
        'u813585',
        'U813943',
        'u813943'
    );

delete from sc_vlci_userdb.public.authorities where authority ='AccEconCiudMoviAlca';
COMMIT;
