-- Revert sc_vlci_userdb:v_migrations/TLF053-cambio_permisos_rol from pg

BEGIN;

ALTER TABLE public.authorities ALTER COLUMN description TYPE varchar(100) USING description::varchar(100);

UPDATE public.authorities SET description='Acceso a CdM Ciudad, Alcaldia, Economico, Movilidad, Geoportal, Sigval y SCC' WHERE authority='director_gestion_datos';
UPDATE public.authorities SET description='Acceso a CdM Ciudad, Alcaldia, Geoportal, Sigval, SCC, Medioambiente y Sensores' WHERE authority='funcionario_oci';
UPDATE public.authorities SET description='Acceso a CdM Ciudad, Alcaldia, Geoportal, Sigval, SCC, Medioambiente, Sensores y Wifi' WHERE authority='funcionario_oci_wifi';
UPDATE public.authorities SET description='Acceso a CdM Ciudad, Alcaldia, Geoportal y Sigval' WHERE authority='funcionario_sertic';
UPDATE public.authorities SET description='Acceso a CdM Ciudad, Geoportal y Sigval' WHERE authority='jefe_servicio_movilidad';
UPDATE public.authorities SET description='Acceso a CdM Ciudad, Alcaldia, Geoportal y Sigval' WHERE authority='jefe_servicio_sertic';
UPDATE public.authorities SET description='Acceso a CdM Ciudad, Alcaldia, Geoportal, Sigval y SCC' WHERE authority='politico_gobierno';

COMMIT;
