-- Revert sc_vlci_userdb:v_migrations/TLF049-nuevos_roles from pg
BEGIN;

------------------- Cambio Cargo y Nombres ---------------------
    UPDATE public.users
        SET
            "position" = 'Funcionario General'
        WHERE
            username in (
                'U16608',
                'u16608',
                'U19537',
                'u19537',
                'U19896',
                'u19896'
            );

        UPDATE public.users
        SET
            "description" = 'Correcher Rigau'
        WHERE
            username in ('U16996', 'u16996');

        UPDATE public.users
        SET
            "description" = 'Magraner Llinares'
        WHERE
            username in ('U19896', 'u19896');

        UPDATE public.users
        SET
            "description" = 'Molla Calabuig'
        WHERE
            username in ('U50496', 'u50496');

--------------------------Crear roles---------------------------
    INSERT INTO
        sc_vlci_userdb.public.authorities (authority, description)
    VALUES
        ('AccCiudad', 'Acceso a CdM Ciudad'),
        (
            'AccCiudadAlcaldia',
            'Acceso a CdM Ciudad y Alcaldia'
        ),
        (
            'AccEconCiudAlcaMedSen',
            'Acceso a CdM Economico, Ciudad, Alcaldia, Medioambiente y Sensores'
        ),
        (
            'AccEconCiudMoviAlca',
            'Acceso a CdM Enonomico, Ciudad, Movilidad y Alcaldia'
        ),
        ('AccMovilidad', 'Acceso a CdM Movilidad'),
        (
            'AccCiudadMovilidad',
            'Acceso a CdM Ciudad y Movilidad'
        ),
        (
            'AccEconCiudMoviAlcaMedSen',
            'Acceso a CdM Economico, Ciudad, Movilidad, Alcaldia, Medioambiente y Sensores'
        ),
        (
            'AccEconCiudMoviAlcaMedSenWif',
            'Acceso a CdM Economico, Ciudad, Movilidad, Alcaldia, Medioambiente, Sensores y Wifi'
        ),
        ('AccEconomico', 'Acceso a CdM Economico'),
        (
            'AccEconMed',
            'Acceso a CdM Economico y Medioambiente'
        ),
        (
            'AccEconomicoMovilidadCiudad',
            'Acceso a CdM Economico, Movilidad y Ciudad'
        ),
        (
            'AccCiudMoviAlca',
            'Acceso a CdM Ciudad, Movilidad y Alcaldia'
        ),
        ('AccEconomicoMovilidad', 'Acceso a CdM Economico y Movilidad');

-------------------- Eliminar Usuarios -----------------------
    insert into
            public.users (
                username,
                password,
                enabled,
                description,
                position
            )
        values
            (
                'UALFAF',
                'Password1',
                1,
                'Paco Albiol Fabregat',
                'Funcionario General'
            ),
            (
                'ualfaf',
                'Password1',
                1,
                'Paco Albiol Fabregat',
                'Funcionario General'
            ),
            (
                'U813647',
                'Password1',
                1,
                'Edurne Badiola',
                'Funcionario OCI'
            ),
            (
                'u813647',
                'Password1',
                1,
                'Edurne Badiola',
                'Funcionario OCI'
            );

        INSERT INTO
            sc_vlci_userdb.public.granted_authorities (username, authority)
        VALUES
            ('ualfaf', 'AccMovilidad'),
            ('UALFAF', 'AccMovilidad'),
            ('U813647', 'DBPentDeveloper'),
            ('u813647', 'DBPentDeveloper');

------------------------DBPentDeveloper-------------------------
    UPDATE public.granted_authorities
    SET
        "authority" = 'AccEconCiudAlcaMedSen'
    WHERE
        username in ('UHEMUD', 'ubamod');

---------------------Director Gestion Datos---------------------
    UPDATE public.granted_authorities
    SET
        "authority" = 'AccEconCiudMoviAlca'
    WHERE
        username in ('U813585', 'u813585', 'U813943', 'u813943');

-------------------------Funcionario Movilidad-------------------------
    UPDATE public.granted_authorities
    SET
        "authority" = 'AccCiudadMovilidad'
    WHERE
        username in (
            'U17994',
            'u17994',
            'U66173',
            'u66173',
            'U67016',
            'u67016',
            'U18218',
            'u18218',
            'U813012',
            'u813012'
        );

----------------------------Funcionario OCI----------------------------
    UPDATE public.granted_authorities
    SET
        "authority" = 'AccEconCiudMoviAlcaMedSen'
    WHERE
        username in (
            'LEFC',
            'lefc',
            'LJPL',
            'ljpl',
            'LRFT',
            'lrft',
            'u13580',
            'U13580',
            'U17569',
            'u17569',
            'U18133',
            'u18133'
        );

--------------------------Funcionario OCI Wifi-------------------------
    UPDATE public.granted_authorities
    SET
        "authority" = 'AccEconCiudMoviAlcaMedSenWif'
    WHERE
        username in ('LJCG', 'ljcg', 'U16656', 'u16656');

---------------------------Jefes de Servicio---------------------------
    UPDATE public.granted_authorities
    SET
        "authority" = 'AccEconomico'
    WHERE
        username in (
            'U16864',
            'u16864',
            'U10807',
            'u10807',
            'U11020',
            'u11020',
            'U16675',
            'u16675',
            'U16678',
            'u16678',
            'U16742',
            'u16742',
            'U16852',
            'u16852',
            'U17389',
            'u17389',
            'U17390',
            'u17390',
            'U17394',
            'u17394',
            'U17398',
            'u17398',
            'U18330',
            'u18330',
            'U19261',
            'u19261',
            'U19441',
            'u19441',
            'U22346',
            'u22346',
            'U22348',
            'u22348',
            'U39892',
            'u39892',
            'U50483',
            'u50483',
            'U17381',
            'u17381',
            'LVRI',
            'lvri',
            'U16608',
            'u16608',
            'U16683',
            'u16683',
            'U16733',
            'u16733',
            'U16863',
            'u16863',
            'U17387',
            'u17387',
            'U17412',
            'u17412',
            'U18304',
            'u18304',
            'U19109',
            'u19109',
            'U19436',
            'u19436',
            'U19479',
            'u19479',
            'U19897',
            'u19897',
            'U47100',
            'u47100',
            'U60051',
            'u60051',
            'USAFEM',
            'usafem',
            'LMAG',
            'lmag',
            'U16016',
            'u16016',
            'U17040',
            'u17040',
            'U18041',
            'u18041',
            'U18427',
            'u18427',
            'U18868',
            'u18868',
            'U19233',
            'u19233',
            'U19370',
            'u19370',
            'U19437',
            'u19437',
            'U19537',
            'u19537',
            'U29661',
            'u29661',
            'U65852',
            'u65852',
            'U10923',
            'u10923',
            'U11007',
            'u11007',
            'U16671',
            'u16671',
            'U16866',
            'u16866',
            'U16928',
            'u16928',
            'U16996',
            'u16996',
            'U17068',
            'u17068',
            'U17963',
            'u17963',
            'U18542',
            'u18542',
            'U19439',
            'u19439',
            'U39078',
            'u39078',
            'U10809',
            'u10809',
            'U16007',
            'u16007',
            'U17385',
            'u17385',
            'U18050',
            'u18050',
            'U18520',
            'u18520',
            'U19442',
            'u19442',
            'U19896',
            'u19896',
            'U21571',
            'u21571',
            'U50496',
            'u50496'
        );

--------------------Jefes de Servicio Medioambiente--------------------
    UPDATE public.granted_authorities
    SET
        "authority" = 'AccEconMed'
    WHERE
        username in ('U17399', 'u17399');

----------------------Jefes de Servicio Movilidad----------------------
    UPDATE public.granted_authorities
    SET
        "authority" = 'AccEconomicoMovilidadCiudad'
    WHERE
        username in ('U19895', 'u19895');

---------------------------Politico Gobierno---------------------------
    UPDATE public.granted_authorities
    SET
        "authority" = 'AccCiudadAlcaldia'
    WHERE
        username in (
            'UCDM1',
            'ucdm1',
            'U18761',
            'u18761',
            'U301362',
            'u301362',
            'U301369',
            'u301369',
            'U813624',
            'u813624'
        );

--------------------------Politico Oposicion---------------------------
    UPDATE public.granted_authorities
    SET
        "authority" = 'AccCiudad'
    WHERE
        username in ('U301335', 'u301335');

    UPDATE public.granted_authorities
    SET
        "authority" = 'DBPentDeveloper'
    WHERE
        username in ('uhemud');

------------------------------Secretario-------------------------------
    UPDATE public.granted_authorities
    SET
        "authority" = 'AccCiudMoviAlca'
    WHERE
        username in (
            'U14585',
            'u14585',
            'U16006',
            'u16006',
            'U16655',
            'u16655',
            'U17625',
            'u17625',
            'U18805',
            'u18805',
            'U813817',
            'u813817'
        );

----------------------------Eliminar roles-----------------------------
    delete from sc_vlci_userdb.public.authorities
    where
        authority in (
            'politico_oposicion',
            'politico_gobierno',
            'director_gestion_datos',
            'funcionario_movilidad',
            'funcionario_oci',
            'funcionario_oci_wifi',
            'jefe_servicio',
            'jefe_servicio_medioambiente',
            'jefe_servicio_movilidad',
            'secretario'
        );        
COMMIT;