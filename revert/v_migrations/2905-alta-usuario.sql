-- Revert sc_vlci_userdb:v_migrations/2905-alta-usuario from pg

BEGIN;

DELETE FROM public.granted_authorities where username in ('U301369', 'u301369');
DELETE FROM public.users where username in ('U301369', 'u301369');

COMMIT;
