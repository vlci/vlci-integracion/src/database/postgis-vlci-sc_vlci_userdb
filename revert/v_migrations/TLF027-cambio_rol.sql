-- Revert sc_vlci_userdb:v_migrations/TLF027-cambio_rol from pg

BEGIN;

delete from sc_vlci_userdb.public.authorities 
	where authority ='AccEconomicoMovilidadCiudad';

UPDATE public.granted_authorities
	SET "authority"='AccEconomicoMovilidad'
	WHERE username in (
        'U19895','u19895'
    );

COMMIT;
