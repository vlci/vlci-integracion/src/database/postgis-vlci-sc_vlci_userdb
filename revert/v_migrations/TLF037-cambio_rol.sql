-- Revert sc_vlci_userdb:v_migrations/TLF037-cambio_rol from pg

BEGIN;

UPDATE public.granted_authorities
	SET "authority"='AccEconomico'
	WHERE username='UHEMUD';

COMMIT;
