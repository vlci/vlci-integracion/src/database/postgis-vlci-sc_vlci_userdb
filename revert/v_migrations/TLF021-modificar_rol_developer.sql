-- Revert sc_vlci_userdb:v_migrations/TLF021-modificar_rol_developer from pg

BEGIN;

    UPDATE sc_vlci_userdb.public.granted_authorities
	SET authority='AccUnificadoEconomico' 
	WHERE username IN (
	    'UPICOL', 'upicol'
	);

	UPDATE sc_vlci_userdb.public.granted_authorities
	SET authority='AccUnifEconCiudMoviAlca' 
	WHERE username IN (
	    'U18667','u18667'
	);

COMMIT;
