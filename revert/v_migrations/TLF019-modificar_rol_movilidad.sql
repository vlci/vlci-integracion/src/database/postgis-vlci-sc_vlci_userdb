-- Revert sc_vlci_userdb:v_migrations/TLF019-modificar_rol_movilidad from pg

BEGIN;

	UPDATE sc_vlci_userdb.public.granted_authorities
	SET authority='AccUnificadoMovilidad' 
	WHERE username IN (
	    'U17994', 'u17994',
	    'U66173', 'u66173',
	    'UALFAF', 'ualfaf'
	);

COMMIT;
