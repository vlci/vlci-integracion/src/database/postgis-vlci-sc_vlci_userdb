-- Revert sc_vlci_userdb:v_migrations/TLF036-alta_usuario from pg

BEGIN;

DELETE FROM public.granted_authorities
	WHERE username in ('UMOJEF','umojef');

DELETE FROM public.users
	WHERE username in ('UMOJEF','umojef');

COMMIT;
