-- Revert sc_vlci_userdb:v_migrations/TLF018-modificar_rol_economicoMovilidad from pg

BEGIN;

	UPDATE sc_vlci_userdb.public.granted_authorities
	SET authority='AccUnificadoEconomicoMovilidad' 
	WHERE username IN (
	    'U19895', 'u19895'
	);

COMMIT;
