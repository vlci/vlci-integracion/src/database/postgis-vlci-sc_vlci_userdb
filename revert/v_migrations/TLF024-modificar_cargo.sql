-- Revert sc_vlci_userdb:v_migrations/TLF024-modificar_cargo from pg

BEGIN;

UPDATE public.users
	SET "position"='Funcionario General'
	WHERE username in (
        'U16016','u16016',
        'U16866','u16866',
        'U17963','u17963',
        'U14585','u14585',
        'U16006','u16006',
        'U16655','u16655',
        'U17625', 'u17625',
        'U18805', 'u18805',
        'U16007','u16007',
        'U18050','u18050',
        'U47100','u47100',
        'U19479','u19479'
    );

COMMIT;
