-- Revert sc_vlci_userdb:v_migrations/TLF050-alta_baja_usuarios from pg

BEGIN;

DELETE FROM public.granted_authorities
	WHERE username in ('U19915','u19915');

DELETE FROM public.users
	WHERE username in ('U19915','u19915');

insert into public.users (username, password, enabled, description, position) 
	values ('LRFT', 'Password1', 1, 'Ramon Ferri Tormo', 'Funcionario OCI'),
    ('lrft', 'Password1', 1, 'Ramon Ferri Tormo', 'Funcionario OCI');

INSERT INTO sc_vlci_userdb.public.granted_authorities (username,authority) 
    VALUES
        ('LRFT','funcionario_oci'),
        ('lrft','funcionario_oci');


--Corrección nombres con espacio al inicio--
    UPDATE public.users
    SET
        "description" = ' Carles Salom'
    WHERE
        username in ('U813943', 'u813943');

    UPDATE public.users
    SET
        "description" = ' Josep Ribes'
    WHERE
        username in ('u813585', 'U813585');
COMMIT;
