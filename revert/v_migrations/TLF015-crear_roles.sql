-- Revert sc_vlci_userdb:v_migrations/TLF015-crear_roles from pg

BEGIN;

delete from sc_vlci_userdb.public.authorities 
	where authority IN (
		'AccEconomico',
		'AccEconomicoMovilidad',
		'AccMovilidad'
	);

COMMIT;
