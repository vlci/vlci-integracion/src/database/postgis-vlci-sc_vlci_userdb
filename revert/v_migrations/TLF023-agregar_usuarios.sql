-- Revert sc_vlci_userdb:v_migrations/TLF023-agregar_usuarios from pg

BEGIN;

	DELETE FROM public.granted_authorities
	WHERE username in ('U19535','u19535');

	DELETE FROM public.users
	WHERE username in ('U19535','u19535');

COMMIT;
