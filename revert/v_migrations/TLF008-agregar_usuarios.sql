-- Revert sc_vlci_userdb:v_migrations/TLF008-agregar_usuarios from pg

BEGIN;

DELETE FROM public.granted_authorities
WHERE username in ('LJPL','ljpl', 'URUSAS','urusas');
DELETE FROM public.users
WHERE username in ('LJPL','ljpl', 'URUSAS','urusas');


COMMIT;
