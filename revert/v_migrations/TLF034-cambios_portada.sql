-- Revert sc_vlci_userdb:v_migrations/TLF034-cambios_portada from pg
BEGIN;

INSERT INTO
    sc_vlci_userdb.public.authorities (authority, description)
VALUES
    (
        'AccEconCiudMoviAlca',
        'Acceso a CdM Economico, Ciudad, Movilidad y Alcaldia'
    );

UPDATE
    sc_vlci_userdb.public.granted_authorities
SET
    authority = 'AccEconCiudMoviAlca'
WHERE
    authority = 'AccEconCiudMoviAlcaMedSen'
    or username IN ('LJCG', 'ljcg', 'U16656', 'u16656');

UPDATE
    sc_vlci_userdb.public.granted_authorities
SET
    authority = 'AccEconomico'
WHERE
    username IN ('U17399', 'u17399');

delete from
    sc_vlci_userdb.public.authorities
where
    authority IN(
        'AccEconCiudMoviAlcaMedSen',
        'AccEconCiudMoviAlcaMedSenWif',
        'AccEconMed'
    );

COMMIT;