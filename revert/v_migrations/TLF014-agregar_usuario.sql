-- Revert sc_vlci_userdb:v_migrations/TLF014-agregar_usuario from pg

BEGIN;

DELETE FROM public.granted_authorities
WHERE username in ('U301335','u301335');

DELETE FROM public.users
WHERE username in ('U301335','u301335');

COMMIT;
