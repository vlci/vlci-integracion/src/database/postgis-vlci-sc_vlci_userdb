-- Revert sc_vlci_userdb:v_migrations/TLF052-agregar_usuarios_rol from pg
BEGIN;

DELETE FROM public.granted_authorities
WHERE
    username in ('LPBG', 'lpbg', 'LEBP', 'lebp');

DELETE FROM public.users
WHERE
    username in ('LPBG', 'lpbg', 'LEBP', 'lebp');

UPDATE public.granted_authorities
SET
    "authority" = 'jefe_servicio'
WHERE
    username in ('LVRI', 'lvri');
    
delete from sc_vlci_userdb.public.authorities
where
    authority in ('funcionario_sertic', 'jefe_servicio_sertic');

COMMIT;