-- Revert sc_vlci_userdb:v_migrations/2246-alta-usuarios-sabri-y-cecilia from pg

BEGIN;

DELETE FROM public.granted_authorities where username in ('UMAMAS', 'umamas', 'UFIROC', 'ufiroc');
DELETE FROM public.users where username in ('UMAMAS', 'umamas', 'UFIROC', 'ufiroc');

COMMIT;
