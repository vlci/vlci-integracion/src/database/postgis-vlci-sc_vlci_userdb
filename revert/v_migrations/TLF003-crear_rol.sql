-- Revert sc_vlci_userdb:v_migrations/TLF003-crear_rol from pg

BEGIN;

delete from sc_vlci_userdb.public.authorities where authority in('DBPentDeveloperParticipacion', 'AccUnifEconCiudMoviAlca');

COMMIT;
