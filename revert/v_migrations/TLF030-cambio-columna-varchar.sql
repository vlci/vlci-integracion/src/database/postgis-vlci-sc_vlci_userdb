-- Revert sc_vlci_userdb:v_migrations/TLF030-cambio-columna-varchar from pg

BEGIN;

DELETE FROM public.granted_authorities
WHERE LOWER(username) = username;

DELETE FROM public.users
WHERE LOWER(username) = username;

ALTER TABLE sc_vlci_userdb.public.granted_authorities ALTER COLUMN username TYPE public.citext;

ALTER TABLE sc_vlci_userdb.public.users 
ALTER COLUMN username TYPE public.citext;

COMMIT;
