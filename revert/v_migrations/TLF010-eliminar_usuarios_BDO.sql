-- Revert sc_vlci_userdb:v_migrations/TLF010-eliminar_usuarios_BDO from pg
BEGIN;

INSERT INTO
    public.users (username, password, enabled, description)
VALUES
    (
        'U10203',
        'Password1',
        1,
        'Herrero Cordero, J. Raúl'
    ),
    (
        'u10203',
        'Password1',
        1,
        'Herrero Cordero, J. Raúl'
    ),
    (
        'U17173',
        'Password1',
        1,
        'Feliciano Albaladejo Olmos'
    ),
    (
        'u17173',
        'Password1',
        1,
        'Feliciano Albaladejo Olmos'
    ),
    (
        'U18048',
        'Password1',
        1,
        'Fco.Xavier Ribera Casado'
    ),
    (
        'u18048',
        'Password1',
        1,
        'Fco.Xavier Ribera Casado'
    ),
    ('U18056', 'Password1', 1, 'Tur Sanahuja'),
    ('u18056', 'Password1', 1, 'Tur Sanahuja'),
    ('U18095', 'Password1', 1, 'Moreno García, Júlia'),
    ('u18095', 'Password1', 1, 'Moreno García, Júlia'),
    ('U18294', 'Password1', 1, 'Carlos Fernández'),
    ('u18294', 'Password1', 1, 'Carlos Fernández'),
    ('U18353', 'Password1', 1, 'Martínez Tormo'),
    ('u18353', 'Password1', 1, 'Martínez Tormo'),
    ('U18758', 'Password1', 1, 'Josep Gimeno'),
    ('u18758', 'Password1', 1, 'Josep Gimeno'),
    ('U18994', 'Password1', 1, 'Ruiz Santosjuanes'),
    ('u18994', 'Password1', 1, 'Ruiz Santosjuanes'),
    ('U19017', 'Password1', 1, 'Mira Gual, Amador'),
    ('u19017', 'Password1', 1, 'Mira Gual, Amador'),
    (
        'U19028',
        'Password1',
        1,
        'Narváez Pérez, Vicenta'
    ),
    (
        'u19028',
        'Password1',
        1,
        'Narváez Pérez, Vicenta'
    ),
    (
        'U301278',
        'Password1',
        1,
        'Rafael Rubio Martinez'
    ),
    (
        'u301278',
        'Password1',
        1,
        'Rafael Rubio Martinez'
    ),
    ('U301323', 'Password1', 1, 'Joan Ribo Canut'),
    ('u301323', 'Password1', 1, 'Joan Ribo Canut'),
    (
        'U301330',
        'Password1',
        1,
        'Florentina Pedrero Gil'
    ),
    (
        'u301330',
        'Password1',
        1,
        'Florentina Pedrero Gil'
    ),
    (
        'U301334',
        'Password1',
        1,
        'Glória Amparo Tello Company'
    ),
    (
        'u301334',
        'Password1',
        1,
        'Glória Amparo Tello Company'
    ),
    (
        'U301335',
        'Password1',
        1,
        'Pere Sixte Fuset I Tortosa'
    ),
    (
        'u301335',
        'Password1',
        1,
        'Pere Sixte Fuset I Tortosa'
    ),
    ('U301336', 'Password1', 1, 'Giuseppe Grezzi'),
    ('u301336', 'Password1', 1, 'Giuseppe Grezzi'),
    (
        'U301337',
        'Password1',
        1,
        'Isabel Lozano Lazaro'
    ),
    (
        'u301337',
        'Password1',
        1,
        'Isabel Lozano Lazaro'
    ),
    (
        'U301338',
        'Password1',
        1,
        'Sergi Campillo Fernandez'
    ),
    (
        'u301338',
        'Password1',
        1,
        'Sergi Campillo Fernandez'
    ),
    (
        'U301339',
        'Password1',
        1,
        'Carlos Galiana Llorens'
    ),
    (
        'u301339',
        'Password1',
        1,
        'Carlos Galiana Llorens'
    ),
    ('U301350', 'Password1', 1, 'Sandra Gomez Lopez'),
    ('u301350', 'Password1', 1, 'Sandra Gomez Lopez'),
    (
        'U301353',
        'Password1',
        1,
        'Jose Vicente Cortes Carreres'
    ),
    (
        'u301353',
        'Password1',
        1,
        'Jose Vicente Cortes Carreres'
    ),
    (
        'U301357',
        'Password1',
        1,
        'Julio Olmos Lablanca'
    ),
    (
        'u301357',
        'Password1',
        1,
        'Julio Olmos Lablanca'
    ),
    (
        'U301363',
        'Password1',
        1,
        'Pilar Bernabé García'
    ),
    (
        'u301363',
        'Password1',
        1,
        'Pilar Bernabé García'
    ),
    ('U301364', 'Password1', 1, 'Aarón Cano Montaner'),
    ('u301364', 'Password1', 1, 'Aarón Cano Montaner'),
    ('U301366', 'Password1', 1, 'Notario Villanueva'),
    ('u301366', 'Password1', 1, 'Notario Villanueva'),
    (
        'U301367',
        'Password1',
        1,
        'Lucía Beamud Villanueva'
    ),
    (
        'u301367',
        'Password1',
        1,
        'Lucía Beamud Villanueva'
    ),
    (
        'U301368',
        'Password1',
        1,
        'Alejandro Ramón Álvarez'
    ),
    (
        'u301368',
        'Password1',
        1,
        'Alejandro Ramón Álvarez'
    ),
    (
        'U301373',
        'Password1',
        1,
        'Emiliano García Domene'
    ),
    (
        'u301373',
        'Password1',
        1,
        'Emiliano García Domene'
    ),
    (
        'U301374',
        'Password1',
        1,
        'Mª Teresa Ibáñez Giménez'
    ),
    (
        'u301374',
        'Password1',
        1,
        'Mª Teresa Ibáñez Giménez'
    ),
    ('U301375', 'Password1', 1, 'Elisa Valía Cotanda'),
    ('u301375', 'Password1', 1, 'Elisa Valía Cotanda'),
    ('U301382', 'Password1', 1, 'Puchades Vila'),
    ('u301382', 'Password1', 1, 'Puchades Vila'),
    ('U301383', 'Password1', 1, 'Borja Sanjuan Roca'),
    ('u301383', 'Password1', 1, 'Borja Sanjuan Roca'),
    ('U301385', 'Password1', 1, 'Elisa Gimeno Gomez'),
    ('u301385', 'Password1', 1, 'Elisa Gimeno Gomez'),
    (
        'U47055',
        'Password1',
        1,
        'José Ignacio Pastor Pérez'
    ),
    (
        'u47055',
        'Password1',
        1,
        'José Ignacio Pastor Pérez'
    ),
    (
        'U50237',
        'Password1',
        1,
        'Jose Manuel Izquierdo Silla'
    ),
    (
        'u50237',
        'Password1',
        1,
        'Jose Manuel Izquierdo Silla'
    ),
    (
        'U60361',
        'Password1',
        1,
        'Roig Lorente, José Enrique'
    ),
    (
        'u60361',
        'Password1',
        1,
        'Roig Lorente, José Enrique'
    ),
    (
        'U65692',
        'Password1',
        1,
        'Silvia Marti Escudero'
    ),
    (
        'u65692',
        'Password1',
        1,
        'Silvia Marti Escudero'
    ),
    ('U74678', 'Password1', 1, 'Calahorro Lizondo'),
    ('u74678', 'Password1', 1, 'Calahorro Lizondo'),
    ('U75416', 'Password1', 1, 'Bayona Cosín'),
    ('u75416', 'Password1', 1, 'Bayona Cosín'),
    ('U75420', 'Password1', 1, 'Calero Rivero'),
    ('u75420', 'Password1', 1, 'Calero Rivero'),
    ('U75471', 'Password1', 1, 'Esteban Espi'),
    ('u75471', 'Password1', 1, 'Esteban Espi'),
    ('U75472', 'Password1', 1, 'Martínez Garrido'),
    ('u75472', 'Password1', 1, 'Martínez Garrido'),
    ('U301347', 'Password1', 1, 'Jordi Peris Blanes'),
    ('u301347', 'Password1', 1, 'Jordi Peris Blanes'),
    ('U19236', 'Password1', 1, 'Jose Villalba Ruiz'),
    ('u19236', 'Password1', 1, 'Jose Villalba Ruiz'),
    ('U301391', 'Password1', 1, 'Yolanda Prats'),
    ('u301391', 'Password1', 1, 'Yolanda Prats'),
    ('U301381', 'Password1', 1, 'Josep Bort Bono'),
    ('u301381', 'Password1', 1, 'Josep Bort Bono'),
    (
        'U19535',
        'Password1',
        1,
        'Vicente Rodrigo Ingresa'
    ),
    (
        'u19535',
        'Password1',
        1,
        'Vicente Rodrigo Ingresa'
    ),
    (
        'UGUAGJ',
        'Password1',
        1,
        'Juan Ramón Gutiérrez Agullo'
    ),
    (
        'uguagj',
        'Password1',
        1,
        'Juan Ramón Gutiérrez Agullo'
    );

INSERT INTO
    public.granted_authorities (username, authority)
VALUES
    ('U17173', 'AccUnificadoEconomicoCiudad'),
    ('u17173', 'AccUnificadoEconomicoCiudad'),
    ('U18048', 'AccUnificadoEconomicoCiudad'),
    ('u18048', 'AccUnificadoEconomicoCiudad'),
    ('U18056', 'AccUnificadoMovilidad'),
    ('u18056', 'AccUnificadoMovilidad'),
    ('U18095', 'AccUnificadoEconomico'),
    ('u18095', 'AccUnificadoEconomico'),
    ('U18294', 'AccUnificadoEconomico'),
    ('u18294', 'AccUnificadoEconomico'),
    ('U18994', 'AccUnificado'),
    ('u18994', 'AccUnificado'),
    ('U19017', 'AccUnificadoEconomico'),
    ('u19017', 'AccUnificadoEconomico'),
    ('U301323', 'AccUnificadoEconomicoCiudad'),
    ('u301323', 'AccUnificadoEconomicoCiudad'),
    ('U301338', 'AccUnificadoEconomicoCiudad'),
    ('u301338', 'AccUnificadoEconomicoCiudad'),
    ('U301350', 'AccUnificadoEconomicoCiudad'),
    ('u301350', 'AccUnificadoEconomicoCiudad'),
    ('U301366', 'AccUnificadoEconomicoCiudad'),
    ('u301366', 'AccUnificadoEconomicoCiudad'),
    ('U301368', 'AccUnificadoEconomicoCiudad'),
    ('u301368', 'AccUnificadoEconomicoCiudad'),
    ('U301373', 'AccUnificadoEconomicoCiudad'),
    ('u301373', 'AccUnificadoEconomicoCiudad'),
    ('U301375', 'AccUnificadoEconomicoCiudad'),
    ('u301375', 'AccUnificadoEconomicoCiudad'),
    ('U301382', 'AccUnificadoEconomico'),
    ('u301382', 'AccUnificadoEconomico'),
    ('U301383', 'AccUnificadoEconomicoCiudad'),
    ('u301383', 'AccUnificadoEconomicoCiudad'),
    ('U47055', 'AccUnificadoEconomico'),
    ('u47055', 'AccUnificadoEconomico'),
    ('U50237', 'AccUnificadoEconomico'),
    ('u50237', 'AccUnificadoEconomico'),
    ('U60361', 'AccUnificadoEconomico'),
    ('u60361', 'AccUnificadoEconomico'),
    ('U65692', 'AccUnificadoEconomico'),
    ('u65692', 'AccUnificadoEconomico'),
    ('U75416', 'AccUnificado'),
    ('u75416', 'AccUnificado'),
    ('U75420', 'AccUnificado'),
    ('u75420', 'AccUnificado'),
    ('U301347', 'AccUnificado'),
    ('u301347', 'AccUnificado'),
    ('U19236', 'AccUnificado'),
    ('u19236', 'AccUnificado'),
    ('U301391', 'AccUnificadoEconomicoCiudad'),
    ('u301391', 'AccUnificadoEconomicoCiudad'),
    ('U10203', 'AccUnificadoEconomico'),
    ('u10203', 'AccUnificadoEconomico'),
    ('U18353', 'AccUnificadoEconomicoCiudadMovilidad'),
    ('u18353', 'AccUnificadoEconomicoCiudadMovilidad'),
    ('U18758', 'AccUnificadoEconomico'),
    ('u18758', 'AccUnificadoEconomico'),
    ('U19028', 'AccUnificadoEconomico'),
    ('u19028', 'AccUnificadoEconomico'),
    ('U301278', 'AccUnificadoEconomico'),
    ('u301278', 'AccUnificadoEconomico'),
    ('U301330', 'AccUnificado'),
    ('u301330', 'AccUnificado'),
    ('U301334', 'AccUnificadoEconomicoCiudad'),
    ('u301334', 'AccUnificadoEconomicoCiudad'),
    (
        'U301336',
        'AccUnificadoEconomicoCiudadMovilidad'
    ),
    (
        'u301336',
        'AccUnificadoEconomicoCiudadMovilidad'
    ),
    ('U301337', 'AccUnificadoEconomicoCiudad'),
    ('u301337', 'AccUnificadoEconomicoCiudad'),
    ('U301339', 'AccUnificadoEconomicoCiudad'),
    ('u301339', 'AccUnificadoEconomicoCiudad'),
    ('U301353', 'AccUnificadoEconomicoCiudad'),
    ('u301353', 'AccUnificadoEconomicoCiudad'),
    ('U301357', 'AccUnificadoEconomico'),
    ('u301357', 'AccUnificadoEconomico'),
    ('U301363', 'AccUnificadoEconomicoCiudad'),
    ('u301363', 'AccUnificadoEconomicoCiudad'),
    ('U301364', 'AccUnificadoEconomicoCiudad'),
    ('u301364', 'AccUnificadoEconomicoCiudad'),
    ('U301367', 'AccUnificadoEconomicoCiudad'),
    ('u301367', 'AccUnificadoEconomicoCiudad'),
    ('U301374', 'AccUnificadoEconomicoCiudad'),
    ('u301374', 'AccUnificadoEconomicoCiudad'),
    ('U301385', 'AccUnificadoEconomico'),
    ('u301385', 'AccUnificadoEconomico'),
    ('U74678', 'AccUnificadoMovilidad'),
    ('u74678', 'AccUnificadoMovilidad'),
    ('U75471', 'AccUnificado'),
    ('u75471', 'AccUnificado'),
    ('U75472', 'AccUnificado'),
    ('u75472', 'AccUnificado'),
    (
        'U301381',
        'AccUnificadoEconomicoCiudadMovilidad'
    ),
    (
        'u301381',
        'AccUnificadoEconomicoCiudadMovilidad'
    ),
    ('U301335', 'DBPentDeveloper'),
    ('u301335', 'DBPentDeveloper'),
    ('U19535', 'AccUnificadoEconomicoCiudad'),
    ('u19535', 'AccUnificadoEconomicoCiudad'),
    ('UGUAGJ', 'DBPentDeveloper'),
    ('uguagj', 'DBPentDeveloper');

COMMIT;