-- Revert sc_vlci_userdb:v_migrations/TLF005-agregar_usuarios from pg

BEGIN;

DELETE FROM public.granted_authorities
WHERE username in ('UBAMOD','ubamod', 'U18133','u18133','U16656','u16656','U17569','u17569','UGUAGJ','uguagj');
DELETE FROM public.users
WHERE username in ('UBAMOD','ubamod', 'U18133','u18133','U16656','u16656','U17569','u17569','UGUAGJ','uguagj');

COMMIT;
