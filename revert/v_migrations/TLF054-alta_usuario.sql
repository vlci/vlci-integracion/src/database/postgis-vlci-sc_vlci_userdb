-- Revert sc_vlci_userdb:v_migrations/TLF054-alta_usuario from pg

BEGIN;

DELETE FROM public.granted_authorities
	WHERE username in ('LMHM','lmhm');

DELETE FROM public.users
	WHERE username in ('LMHM','lmhm');

COMMIT;
