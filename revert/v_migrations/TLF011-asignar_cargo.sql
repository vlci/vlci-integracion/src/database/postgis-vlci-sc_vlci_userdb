-- Revert sc_vlci_userdb:v_migrations/TLF011-asignar_cargo from pg
BEGIN;

UPDATE
    public.users
SET
    position = NULL;

COMMIT;