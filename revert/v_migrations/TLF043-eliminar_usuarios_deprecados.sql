-- Revert sc_vlci_userdb:v_migrations/TLF043-eliminar_usuarios_deprecados from pg
BEGIN;

INSERT INTO public.users (username, "password", enabled, description, "position") 
VALUES
('U19535', 'Password1', 1, 'Vicente Rodrigo Ingresa', 'Jefe de Servicio'),
('U10917', 'Password1', 1, 'Maria Elvira Brull Codoñer', 'Jefe de servicio'),
('U16516', 'Password1', 1, 'M Francisca Tamarit Valles', 'Jefe de servicio'),
('U16680', 'Password1', 1, 'Maruxa Ramírez Íñiguez De La Torre', 'Jefe de servicio'),
('U18175', 'Password1', 1, 'Jordi Verdu Benavent', 'Jefe de servicio'),
('U18259', 'Password1', 1, 'García Manzanares, M.ª Mercedes', 'Jefe de servicio'),
('U18452', 'Password1', 1, 'Enrique Manuel Peris Navarro', 'Jefe de servicio'),
('U18541', 'Password1', 1, 'Mateu Cerda, Maria Amparo', 'Jefe de servicio'),
('U19898', 'Password1', 1, 'Enrique Encarnacion Tarazona', 'Jefe de servicio'),
('U39098', 'Password1', 1, 'Fermin Quero De Lera', 'Jefe de servicio'),
('U60334', 'Password1', 1, 'Llopis Alandi, Antonio V.', 'Jefe de servicio'),
('U60350', 'Password1', 1, 'Amelia Quintana Trenor', 'Jefe de servicio'),
('U65701', 'Password1', 1, 'González Giménez, Ana M.ª', 'Jefe de servicio'),
('U65944', 'Password1', 1, 'Bueno Cañigral, Francisco Jesús', 'Jefe de servicio'),
('u19535', 'Password1', 1, 'Vicente Rodrigo Ingresa', 'Jefe de servicio'),
('u10917', 'Password1', 1, 'Maria Elvira Brull Codoñer', 'Jefe de servicio'),
('u16516', 'Password1', 1, 'M Francisca Tamarit Valles', 'Jefe de servicio'),
('u16680', 'Password1', 1, 'Maruxa Ramírez Íñiguez De La Torre', 'Jefe de servicio'),
('u18175', 'Password1', 1, 'Jordi Verdu Benavent', 'Jefe de servicio'),
('u18259', 'Password1', 1, 'García Manzanares, M.ª Mercedes', 'Jefe de servicio'),
('u18452', 'Password1', 1, 'Enrique Manuel Peris Navarro', 'Jefe de servicio'),
('u18541', 'Password1', 1, 'Mateu Cerda, Maria Amparo', 'Jefe de servicio'),
('u19898', 'Password1', 1, 'Enrique Encarnacion Tarazona', 'Jefe de servicio'),
('u39098', 'Password1', 1, 'Fermin Quero De Lera', 'Jefe de servicio'),
('u60334', 'Password1', 1, 'Llopis Alandi, Antonio V.', 'Jefe de servicio'),
('u60350', 'Password1', 1, 'Amelia Quintana Trenor', 'Jefe de servicio'),
('u65701', 'Password1', 1, 'González Giménez, Ana M.ª', 'Jefe de servicio'),
('u65944', 'Password1', 1, 'Bueno Cañigral, Francisco Jesús', 'Jefe de servicio');


INSERT INTO public.granted_authorities (username, authority) 
VALUES
    ('U18259', 'AccEconomico'),
    ('U19898', 'AccEconomico'),
    ('U60334', 'AccEconomico'),
    ('U19535', 'AccEconomico'),
    ('U16516', 'AccEconomico'),
    ('U16680', 'AccEconomico'),
    ('U18175', 'AccEconomico'),
    ('U60350', 'AccEconomico'),
    ('U65701', 'AccEconomico'),
    ('U18452', 'AccEconomico'),
    ('U18541', 'AccEconomico'),
    ('U39098', 'AccEconomico'),
    ('U10917', 'AccEconomico'),
    ('U65944', 'AccEconomico'),
    ('u18259', 'AccEconomico'),
    ('u19898', 'AccEconomico'),
    ('u60334', 'AccEconomico'),
    ('u19535', 'AccEconomico'),
    ('u16516', 'AccEconomico'),
    ('u16680', 'AccEconomico'),
    ('u18175', 'AccEconomico'),
    ('u60350', 'AccEconomico'),
    ('u65701', 'AccEconomico'),
    ('u18452', 'AccEconomico'),
    ('u18541', 'AccEconomico'),
    ('u39098', 'AccEconomico'),
    ('u10917', 'AccEconomico'),
    ('u65944', 'AccEconomico');

COMMIT;