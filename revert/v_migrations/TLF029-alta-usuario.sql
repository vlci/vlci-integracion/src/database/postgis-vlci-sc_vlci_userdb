-- Revert sc_vlci_userdb:v_migrations/TLF029-alta-usuario from pg

BEGIN;

DELETE FROM public.granted_authorities
	WHERE username = 'U813624';

DELETE FROM public.users
	WHERE username = 'U813624';

COMMIT;
