-- Revert sc_vlci_userdb:v_migrations/TLF040-eliminar_usuario from pg
BEGIN;

insert into
    public.users (
        username,
        password,
        enabled,
        description,
        position
    )
values
    ('UMAMAS', 'Password1', 1, 'Sabri Manai', 'Idrica'),
    ('umamas', 'Password1', 1, 'Sabri Manai', 'Idrica');
insert into
    public.granted_authorities (username, authority)
values
    ('UMAMAS', 'DBPentDeveloper'),
    ('umamas', 'DBPentDeveloper');
    COMMIT;