-- Revert sc_vlci_userdb:v_migrations/1673-creacion-usuarios from pg

BEGIN;

update sc_vlci_userdb.public.granted_authorities set authority  = 'AccUnificado' where username = 'U19316';
delete from sc_vlci_userdb.public.granted_authorities where username = 'U18587';
delete from sc_vlci_userdb.public.users where username = 'U18587';
COMMIT;

