-- Revert sc_vlci_userdb:v_migrations/TLF031-alta-modificacion-usuarios from pg

BEGIN;

UPDATE public.granted_authorities
	SET "authority"='AccEconomicoMovilidad'
	WHERE username= 'u19895';

UPDATE public.granted_authorities
	SET "authority"='AccMovilidad'
	WHERE username in ('U66173','u66173', 'U17994','u17994');


DELETE FROM public.granted_authorities
WHERE username in ('U67016','u67016', 'U18218','u18218');

DELETE FROM public.users
WHERE username in ('U67016','u67016', 'U18218','u18218');

delete from sc_vlci_userdb.public.authorities 
	where authority ='AccCiudadMovilidad';
COMMIT;
