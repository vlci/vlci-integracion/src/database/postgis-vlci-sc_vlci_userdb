-- Revert sc_vlci_userdb:v_migrations/TLF044-alta_usuarios from pg

BEGIN;
DELETE FROM public.granted_authorities
	WHERE username in ('U813585','u813585','U813943','u813943');

DELETE FROM public.users
	WHERE username in ('U813585','u813585','U813943','u813943');

COMMIT;
