-- Revert sc_vlci_userdb:v_migrations/TLF051-cambio_rol from pg
BEGIN;

UPDATE public.granted_authorities
SET
    "authority" = 'politico_oposicion'
WHERE
    username = 'uhemud';

UPDATE public.granted_authorities
SET
    "authority" = 'DBPentDeveloper'
WHERE
    username = 'ubamod';

COMMIT;