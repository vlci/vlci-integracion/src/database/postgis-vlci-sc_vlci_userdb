-- Revert sc_vlci_userdb:v_migrations/TLF025-agregar_usuario from pg

BEGIN;

DELETE FROM public.granted_authorities
	WHERE username in ('U813817','u813817');

	DELETE FROM public.users
	WHERE username in ('U813817','u813817');

COMMIT;
