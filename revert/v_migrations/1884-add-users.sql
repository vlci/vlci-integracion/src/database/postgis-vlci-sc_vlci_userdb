-- Revert sc_vlci_userdb:v_migrations/1884-add-users from pg

BEGIN;

DELETE FROM public.granted_authorities where username in ('U18761', 'U301362');
DELETE FROM public.users where username in ('U18761', 'U301362');

COMMIT;
