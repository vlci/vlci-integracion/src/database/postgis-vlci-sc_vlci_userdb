-- Revert sc_vlci_userdb:v_migrations/TLF048-alta_usuario from pg

BEGIN;

DELETE FROM public.granted_authorities
WHERE username in ('ULIMAM','ulimam');

DELETE FROM public.users
WHERE username in ('ULIMAM','ulimam');

COMMIT;
