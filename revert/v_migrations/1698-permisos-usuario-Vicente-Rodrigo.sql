-- Revert sc_vlci_userdb:v_migrations/1698-permisos-usuario-Vicente-Rodrigo from pg

BEGIN;

delete from sc_vlci_userdb.public.granted_authorities where username = 'U19535';
delete from sc_vlci_userdb.public.granted_authorities where username = 'u19535';
delete from sc_vlci_userdb.public.users where username = 'U19535';
delete from sc_vlci_userdb.public.users where username = 'u19535';

UPDATE sc_vlci_userdb.public.granted_authorities
SET authority='AccUnificadoEconomico' where username='LVRI';

UPDATE sc_vlci_userdb.public.granted_authorities
SET authority='AccUnificadoEconomico' where username='lvri';


COMMIT;
