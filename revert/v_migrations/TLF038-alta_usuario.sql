-- Revert sc_vlci_userdb:v_migrations/TLF038-alta_usuario from pg

BEGIN;

DELETE FROM public.granted_authorities
	WHERE username in ('UORICJ','uoricj', 'UCDM1', 'ucdm1');

DELETE FROM public.users
	WHERE username in ('UORICJ','uoricj','UCDM1','ucdm1');

UPDATE public.users
	SET "position"='Alcaldesa'
	WHERE username in ('U301369','u301369');  
COMMIT;
