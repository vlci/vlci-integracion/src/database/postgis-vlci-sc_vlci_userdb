-- Revert sc_vlci_userdb:v_migrations/TLF042-cambiar_rol_usuario from pg

BEGIN;

UPDATE public.granted_authorities
	SET "authority"='AccEconCiudMoviAlcaMedSen'
	WHERE username in (
        'U811919','u811919'
    );

UPDATE public.users
	SET "position"='Funcionario General'
	WHERE username in (
        'U811919','u811919'
    );
COMMIT;
