-- Revert sc_vlci_userdb:v_migrations/TLF026-agregar_usuario from pg

BEGIN;

DELETE FROM public.granted_authorities
	WHERE username in ('U16864','u16864');

DELETE FROM public.users
	WHERE username in ('U16864','u16864');

COMMIT;
