-- Revert sc_vlci_userdb:v_migrations/TLF004-cambiar_rol_usuarios from pg

BEGIN;

UPDATE sc_vlci_userdb.public.granted_authorities
SET authority='AccUnificadoEconomicoCiudadMovilidad' where username in('U301362','u301362','U18761','u18761','U18667','u18667','U13580','u13580');

COMMIT;
