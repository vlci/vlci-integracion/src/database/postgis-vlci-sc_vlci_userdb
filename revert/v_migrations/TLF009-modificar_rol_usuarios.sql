-- Revert sc_vlci_userdb:v_migrations/TLF009-modificar_rol_usuarios from pg

BEGIN;

UPDATE sc_vlci_userdb.public.granted_authorities
SET authority='AccUnificadoEconomicoCiudadMovilidad' where username in('LJCG','ljcg');

COMMIT;
