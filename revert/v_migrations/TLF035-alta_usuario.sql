-- Revert sc_vlci_userdb:v_migrations/TLF035-alta_usuario from pg

BEGIN;

DELETE FROM public.granted_authorities
	WHERE username in ('U813012','u813012');

DELETE FROM public.users
	WHERE username in ('U813012','u813012');

COMMIT;
