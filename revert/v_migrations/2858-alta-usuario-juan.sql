-- Revert sc_vlci_userdb:v_migrations/2858-alta-usuario-juan from pg

BEGIN;

DELETE FROM public.granted_authorities where username in ('ULULOJ', 'ululoj');
DELETE FROM public.users where username in ('ULULOJ', 'ululoj');

COMMIT;
