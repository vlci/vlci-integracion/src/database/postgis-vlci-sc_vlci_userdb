-- Revert sc_vlci_userdb:v_migrations/TLF022-eliminar_usuarios from pg

BEGIN;

	INSERT INTO sc_vlci_userdb.public.users (username, password, enabled, description, position)
	VALUES 
		('u13513', 'Password1', 1, 'Añó I Garcia, Vicent', 'Funcionario General'),
		('U13513', 'Password1', 1, 'Añó I Garcia, Vicent', 'Funcionario General'),
		('u14772', 'Password1', 1, 'Francisco Jose Chillida Marco', 'Funcionario General'),
		('U14772', 'Password1', 1, 'Francisco Jose Chillida Marco', 'Funcionario General'),
		('u16572', 'Password1', 1, 'Valverde Lopez', 'Funcionario General'),
		('U16572', 'Password1', 1, 'Valverde Lopez', 'Funcionario General'),
		('u16864', 'Password1', 1, 'Asunción Abad Tora', 'Funcionario General'),
		('U16864', 'Password1', 1, 'Asunción Abad Tora', 'Funcionario General'),
		('u17392', 'Password1', 1, 'Maria Jose Iranzo Iranzo', 'Funcionario General'),
		('U17392', 'Password1', 1, 'Maria Jose Iranzo Iranzo', 'Funcionario General'),
		('u18336', 'Password1', 1, 'Maria Milagros Ortiz Torremocha', 'Funcionario General'),
		('U18336', 'Password1', 1, 'Maria Milagros Ortiz Torremocha', 'Funcionario General'),
		('u18533', 'Password1', 1, 'Ana Maria Sotelo Puchalt', 'Funcionario General'),
		('U18533', 'Password1', 1, 'Ana Maria Sotelo Puchalt', 'Funcionario General'),
		('U18587', 'Password1', 1, 'Pilar De la Torre Fornes', 'Jefe de servicio'),
		('u19316', 'Password1', 1, 'Fernando Gallego Garcia', 'Funcionario General'),
		('U19316', 'Password1', 1, 'Fernando Gallego Garcia', 'Funcionario General'),
		('u19488', 'Password1', 1, 'Fernando Martinez Lianes', 'Funcionario General'),
		('U19488', 'Password1', 1, 'Fernando Martinez Lianes', 'Funcionario General'),
		('u19492', 'Password1', 1, 'Jose Vicente Sanchez-Tarazaga Marcelino', 'Funcionario General'),
		('U19492', 'Password1', 1, 'Jose Vicente Sanchez-Tarazaga Marcelino', 'Funcionario General'),
		('u19946', 'Password1', 1, 'Vicente Jose Salvador Escoto', 'Funcionario General'),
		('U19946', 'Password1', 1, 'Vicente Jose Salvador Escoto', 'Funcionario General'),
		('u22529', 'Password1', 1, 'Jose Manuel Garcia Garcia', 'Funcionario General'),
		('U22529', 'Password1', 1, 'Jose Manuel Garcia Garcia', 'Funcionario General'),
		('u65159', 'Password1', 1, 'López Gálvez, Jesús Rafael', 'Funcionario General'),
		('U65159', 'Password1', 1, 'López Gálvez, Jesús Rafael', 'Funcionario General'),
		('u66220', 'Password1', 1, 'Cerda Gargallo', 'Funcionario General'),
		('U66220', 'Password1', 1, 'Cerda Gargallo', 'Funcionario General'),
		('u70096', 'Password1', 1, 'Rodriguez Ciria', 'Funcionario General'),
		('U70096', 'Password1', 1, 'Rodriguez Ciria', 'Funcionario General'),
		('usacec', 'Password1', 1, 'Carlos Sánchez Cerveró', 'Funcionario General'),
		('USACEC', 'Password1', 1, 'Carlos Sánchez Cerveró', 'Funcionario General'),
		('uyefej', 'Password1', 1, 'Yerro Fernández', 'Funcionario General'),
		('UYEFEJ', 'Password1', 1, 'Yerro Fernández', 'Funcionario General');

	INSERT INTO sc_vlci_userdb.public.granted_authorities (username, authority)
	VALUES 
		('u13513', 'AccUnificadoEconomico'), ('U13513', 'AccUnificadoEconomico'),
		('u14772', 'AccUnificado'), ('U14772', 'AccUnificado'),
		('u16572', 'AccUnificado'), ('U16572', 'AccUnificado'),
		('u16864', 'AccUnificadoEconomico'), ('U16864', 'AccUnificadoEconomico'),
		('u17392', 'AccUnificadoEconomico'), ('U17392', 'AccUnificadoEconomico'),
		('u18336', 'AccUnificadoEconomico'), ('U18336', 'AccUnificadoEconomico'),
		('u18533', 'AccUnificado'), ('U18533', 'AccUnificado'),
		('U18587', 'AccUnificadoEconomicoCiudadMovilidad'),
		('u19316', 'AccUnificado'), ('U19316', 'AccUnificadoEconomicoCiudadMovilidad'),
		('u19488', 'AccUnificado'), ('U19488', 'AccUnificado'),
		('u19492', 'AccUnificado'), ('U19492', 'AccUnificado'),
		('u19946', 'AccUnificado'),	('U19946', 'AccUnificado'),
		('u22529', 'AccUnificado'), ('U22529', 'AccUnificado'),
		('u65159', 'AccUnificadoEconomico'), ('U65159', 'AccUnificadoEconomico'),
		('u66220', 'AccUnificado'),	('U66220', 'AccUnificado'),
		('u70096', 'AccUnificado'), ('U70096', 'AccUnificado'),
		('usacec', 'AccUnificado'),	('USACEC', 'AccUnificado'),
		('uyefej', 'AccUnificado'),	('UYEFEJ', 'AccUnificado');
COMMIT;
