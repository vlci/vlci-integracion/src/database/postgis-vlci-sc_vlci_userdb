-- Revert sc_vlci_userdb:v_migrations/TLF012-renombrar_usuarios from pg
BEGIN;

UPDATE
    public.users
SET
    description = 'Chillida Marco'
WHERE
    username in ('U14772', 'u14772');

UPDATE
    public.users
SET
    description = 'Ferri Tormo'
WHERE
    username in ('LRFT', 'lrft');

UPDATE
    public.users
SET
    description = 'Garcia Garcia'
WHERE
    username in ('U22529', 'u22529');

UPDATE
    public.users
SET
    description = 'Gines Buendia'
WHERE
    username in ('U17994', 'u17994');

UPDATE
    public.users
SET
    description = 'Jesus'
WHERE
    username in ('U66173', 'u66173');

COMMIT;