-- Revert sc_vlci_userdb:v_migrations/TLF041-cambiar_rol_usuario from pg

BEGIN;

UPDATE public.granted_authorities
	SET "authority"='AccEconCiudMoviAlcaMedSen'
	WHERE username in (
        'U813624','u813624'
    );

COMMIT;
