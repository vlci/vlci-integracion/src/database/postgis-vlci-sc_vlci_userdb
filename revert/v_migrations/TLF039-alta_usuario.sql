-- Revert sc_vlci_userdb:v_migrations/TLF039-alta_usuario from pg

BEGIN;

DELETE FROM public.granted_authorities
	WHERE username in ('U811919','u811919');

DELETE FROM public.users
	WHERE username in ('U811919','u811919');

COMMIT;
