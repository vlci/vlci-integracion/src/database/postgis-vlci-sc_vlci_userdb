-- Revert sc_vlci_userdb:v_migrations/TLF032-alta-usuario from pg

BEGIN;

DELETE FROM public.granted_authorities
	WHERE username in ('U813647','u813647');

DELETE FROM public.users
	WHERE username in ('U813647','u813647');

COMMIT;
