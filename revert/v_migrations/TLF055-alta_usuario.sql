-- Revert sc_vlci_userdb:v_migrations/TLF055-alta_usuario from pg

BEGIN;

DELETE FROM public.granted_authorities
	WHERE username in ('UPICEI','upicei');

DELETE FROM public.users
	WHERE username in ('UPICEI','upicei');

COMMIT;
