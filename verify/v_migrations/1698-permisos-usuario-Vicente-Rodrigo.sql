-- Verify sc_vlci_userdb:v_migrations/1698-permisos-usuario-Vicente-Rodrigo on pg

BEGIN;

select 1/verify.outvalue from (
select
    CASE
        WHEN (count(*) = 335) THEN 1
        ELSE 0
    END as outvalue
from  sc_vlci_userdb.public.users
) verify;
select 1/verify.outvalue from (
select
    CASE
        WHEN (count(*) = 339) THEN 1
        ELSE 0
    END as outvalue
from  sc_vlci_userdb.public.granted_authorities
) verify;

select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username = 'LVRI' and authority = 'AccUnificadoEconomicoCiudad';

select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username = 'lvri' and authority = 'AccUnificadoEconomicoCiudad';

ROLLBACK;
