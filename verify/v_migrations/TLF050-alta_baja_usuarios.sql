-- Verify sc_vlci_userdb:v_migrations/TLF050-alta_baja_usuarios on pg

BEGIN;

    SELECT
        1 / CASE
            WHEN COUNT(*) = 232 THEN 1
            ELSE 0
        END
    FROM
        public.users;

    SELECT
        1 / CASE
            WHEN COUNT(*) = 232 THEN 1
            ELSE 0
        END
    FROM
        public.granted_authorities;

ROLLBACK;
