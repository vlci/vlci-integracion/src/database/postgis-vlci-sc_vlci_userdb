-- Verify sc_vlci_userdb:v_migrations/TLF003-crear_rol on pg

BEGIN;

select 1/count(*) from sc_vlci_userdb.public.authorities where authority='DBPentDeveloperParticipacion';
select 1/count(*) from sc_vlci_userdb.public.authorities where authority='AccUnifEconCiudMoviAlca';

ROLLBACK;
