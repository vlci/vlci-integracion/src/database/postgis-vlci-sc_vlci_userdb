-- Verify sc_vlci_userdb:v_migrations/TLF021-modificar_rol_developer on pg

BEGIN;

    select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='UPICOL' and authority='DBPentDeveloper';
	select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='upicol' and authority='DBPentDeveloper';
	select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='U18667' and authority='DBPentDeveloper';
	select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='u18667' and authority='DBPentDeveloper';

ROLLBACK;
