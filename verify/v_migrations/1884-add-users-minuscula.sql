-- Verify sc_vlci_userdb:v_migrations/1884-add-users-minuscula on pg

BEGIN;


select 1/count(*) from public.users WHERE username='u301362';
select 1/count(*) from public.users WHERE username='u18761';

select 1/count(*) from public.granted_authorities WHERE username='u301362';
select 1/count(*) from public.granted_authorities WHERE username='u18761';

ROLLBACK;
