-- Verify sc_vlci_userdb:v_migrations/TLF024-modificar_cargo on pg

BEGIN;

	select 1/count(*) from sc_vlci_userdb.public.users where username='U16016' and position='Funcionario General/Intervencion';
	select 1/count(*) from sc_vlci_userdb.public.users where username='u16016' and position='Funcionario General/Intervencion';
	select 1/count(*) from sc_vlci_userdb.public.users where username='U16866' and position='Funcionario General/Intervencion';
	select 1/count(*) from sc_vlci_userdb.public.users where username='u16866' and position='Funcionario General/Intervencion';
	select 1/count(*) from sc_vlci_userdb.public.users where username='U17963' and position='Funcionario General/Intervencion';
	select 1/count(*) from sc_vlci_userdb.public.users where username='u17963' and position='Funcionario General/Intervencion';
    
    select 1/count(*) from sc_vlci_userdb.public.users where username='U14585' and position='Funcionario General/Secretario';
    select 1/count(*) from sc_vlci_userdb.public.users where username='u14585' and position='Funcionario General/Secretario';
    select 1/count(*) from sc_vlci_userdb.public.users where username='U16006' and position='Funcionario General/Secretario';
    select 1/count(*) from sc_vlci_userdb.public.users where username='u16006' and position='Funcionario General/Secretario';
    select 1/count(*) from sc_vlci_userdb.public.users where username='U16655' and position='Funcionario General/Secretario';
    select 1/count(*) from sc_vlci_userdb.public.users where username='u16655' and position='Funcionario General/Secretario';
    select 1/count(*) from sc_vlci_userdb.public.users where username='U17625' and position='Funcionario General/Secretario';
    select 1/count(*) from sc_vlci_userdb.public.users where username='u17625' and position='Funcionario General/Secretario';
    select 1/count(*) from sc_vlci_userdb.public.users where username='U18805' and position='Funcionario General/Secretario';
    select 1/count(*) from sc_vlci_userdb.public.users where username='u18805' and position='Funcionario General/Secretario';

    select 1/count(*) from sc_vlci_userdb.public.users where username='U16007' and position='Funcionario General/Tesoreria';
    select 1/count(*) from sc_vlci_userdb.public.users where username='u16007' and position='Funcionario General/Tesoreria';
    select 1/count(*) from sc_vlci_userdb.public.users where username='U18050' and position='Funcionario General/Tesoreria';
    select 1/count(*) from sc_vlci_userdb.public.users where username='u18050' and position='Funcionario General/Tesoreria';

    select 1/count(*) from sc_vlci_userdb.public.users where username='U19479' and position='Jefe de Seccion Adjunto';
    select 1/count(*) from sc_vlci_userdb.public.users where username='u19479' and position='Jefe de Seccion Adjunto';

ROLLBACK;
