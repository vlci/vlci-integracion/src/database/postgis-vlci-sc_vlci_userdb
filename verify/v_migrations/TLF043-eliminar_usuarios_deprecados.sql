-- Verify sc_vlci_userdb:v_migrations/TLF043-eliminar_usuarios_deprecados on pg

BEGIN;

SELECT
	1 / CASE
		WHEN COUNT(*) = 230 THEN 1
		ELSE 0
	END
FROM
	public.users;

SELECT
	1 / CASE
		WHEN COUNT(*) = 230 THEN 1
		ELSE 0
	END
FROM
	public.granted_authorities;

ROLLBACK;
