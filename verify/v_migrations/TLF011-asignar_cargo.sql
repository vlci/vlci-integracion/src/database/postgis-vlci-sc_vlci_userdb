-- Verify sc_vlci_userdb:v_migrations/TLF011-asignar_cargo on pg
BEGIN;

WITH cte AS(
    SELECT
        CASE
            WHEN COUNT(*) != 0 THEN 0
            ELSE 1
        END AS count_result
    FROM
        public.users
    where
        "position" isnull
)
SELECT
    1 / count_result
FROM
    cte;

ROLLBACK;