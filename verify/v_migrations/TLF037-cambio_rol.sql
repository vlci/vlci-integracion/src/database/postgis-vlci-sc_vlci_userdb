-- Verify sc_vlci_userdb:v_migrations/TLF037-cambio_rol on pg

BEGIN;

select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='UHEMUD' and authority='AccEconCiudMoviAlcaMedSen';

ROLLBACK;
