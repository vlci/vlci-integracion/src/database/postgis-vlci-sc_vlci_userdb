-- Verify sc_vlci_userdb:v_migrations/2905-cambiar-rol-usuarios on pg

BEGIN;

select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='U18761' and authority='AccEconCiudAlcaMedSen';
select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='u18761' and authority='AccEconCiudAlcaMedSen';

select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='U301362' and authority='AccEconCiudAlcaMedSen';
select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='u301362' and authority='AccEconCiudAlcaMedSen';

ROLLBACK;
