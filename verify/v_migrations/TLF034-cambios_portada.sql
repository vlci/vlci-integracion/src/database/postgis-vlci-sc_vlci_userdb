-- Verify sc_vlci_userdb:v_migrations/TLF034-cambios_portada on pg
BEGIN;

--Nuevos roles
SELECT
    1 / CASE
        WHEN COUNT(*) = 13 THEN 1
        ELSE 0
    END
FROM
    public.authorities;

--Rol deprecado
SELECT
    1 / CASE
        WHEN COUNT(*) = 0 THEN 1
        ELSE 0
    END
FROM
    public.granted_authorities
where
    authority = 'AccEconCiudMoviAlca';

select
    1 / count(*)
from
    sc_vlci_userdb.public.granted_authorities
where
    username = 'LJCG'
    and authority = 'AccEconCiudMoviAlcaMedSenWif';

select
    1 / count(*)
from
    sc_vlci_userdb.public.granted_authorities
where
    username = 'ljcg'
    and authority = 'AccEconCiudMoviAlcaMedSenWif';

select
    1 / count(*)
from
    sc_vlci_userdb.public.granted_authorities
where
    username = 'U16656'
    and authority = 'AccEconCiudMoviAlcaMedSenWif';

select
    1 / count(*)
from
    sc_vlci_userdb.public.granted_authorities
where
    username = 'u16656'
    and authority = 'AccEconCiudMoviAlcaMedSenWif';

select
    1 / count(*)
from
    sc_vlci_userdb.public.granted_authorities
where
    username = 'U17399'
    and authority = 'AccEconMed';

select
    1 / count(*)
from
    sc_vlci_userdb.public.granted_authorities
where
    username = 'u17399'
    and authority = 'AccEconMed';

ROLLBACK;