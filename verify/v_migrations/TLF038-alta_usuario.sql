-- Verify sc_vlci_userdb:v_migrations/TLF038-alta_usuario on pg
BEGIN;

SELECT
	1 / CASE
		WHEN COUNT(*) = 258 THEN 1
		ELSE 0
	END
FROM
	public.users;

SELECT
	1 / CASE
		WHEN COUNT(*) = 258 THEN 1
		ELSE 0
	END
FROM
	public.granted_authorities;

SELECT
	1 / CASE
		WHEN "position" = 'Politico Gobierno' THEN 1
		ELSE 0
	END
FROM
	public.users
WHERE
	username IN ('U301369', 'u301369');

ROLLBACK;