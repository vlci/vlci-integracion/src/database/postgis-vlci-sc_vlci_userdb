-- Verify sc_vlci_userdb:v_migrations/TLF019-modificar_rol_movilidad on pg

BEGIN;

	select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='U17994' and authority='AccMovilidad';
	select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='u17994' and authority='AccMovilidad';
	select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='U66173' and authority='AccMovilidad';
	select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='u66173' and authority='AccMovilidad';
	select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='UALFAF' and authority='AccMovilidad';
	select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='ualfaf' and authority='AccMovilidad';
	
ROLLBACK;
