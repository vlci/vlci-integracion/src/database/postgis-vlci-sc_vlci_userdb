-- Verify sc_vlci_userdb:v_migrations/TLF042-cambiar_rol_usuario on pg
BEGIN;

select
    1 / case
        when count(*) = 22 then 1
        else 0
    end
from
    public.users
where
    position = 'Funcionario OCI';

select
    1 / case
        when count(*) = 34 then 1
        else 0
    end
from
    public.granted_authorities
where
    authority = 'DBPentDeveloper';

ROLLBACK;