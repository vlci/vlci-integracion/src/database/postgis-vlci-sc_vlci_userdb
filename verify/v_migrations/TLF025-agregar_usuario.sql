-- Verify sc_vlci_userdb:v_migrations/TLF025-agregar_usuario on pg

BEGIN;

SELECT 1/
	CASE 
		WHEN COUNT(*) = 236 THEN 1 
		ELSE 0 
	END
	FROM public.users;

SELECT 1/
	CASE 
		WHEN COUNT(*) = 236 THEN 1 
		ELSE 0 
	END
	FROM public.granted_authorities;

ROLLBACK;
