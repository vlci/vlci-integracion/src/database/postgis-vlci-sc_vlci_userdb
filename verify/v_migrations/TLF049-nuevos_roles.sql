-- Verify sc_vlci_userdb:v_migrations/TLF049-nuevos_roles on pg
BEGIN;

-------------------- Eliminar Usuarios -----------------------
    SELECT
        1 / CASE
            WHEN COUNT(*) = 232 THEN 1
            ELSE 0
        END
    FROM
        public.users;

    SELECT
        1 / CASE
            WHEN COUNT(*) = 232 THEN 1
            ELSE 0
        END
    FROM
        public.granted_authorities;

------------------- Cambio Cargo y Nombres ---------------------
    SELECT
        1 / CASE
            WHEN COUNT(*) = 6 THEN 1
            ELSE 0
        END
    FROM
        public.users
    where
        description in ('Marc Correcher Rigau', 'Maria Jose Magraner Llinares', 'Antonio Molla Calabuig');

    SELECT
        1 / CASE
            WHEN COUNT(*) = 120 THEN 1
            ELSE 0
        END
    FROM
        public.users
    where
        position = 'Jefe de servicio';

------------------------DBPentDeveloper-------------------------
    select
        1 / case
            when count(*) = 35 then 1
            else 0
        end
    from
        public.granted_authorities
    where
        authority = 'DBPentDeveloper';

---------------------Director Gestion Datos---------------------
    select
        1 / case
            when count(*) = 4 then 1
            else 0
        end
    from
        public.granted_authorities
    where
        authority = 'director_gestion_datos';

-------------------------Funcionario Movilidad-------------------------
    select
        1 / case
            when count(*) = 10 then 1
            else 0
        end
    from
        public.granted_authorities
    where
        authority = 'funcionario_movilidad';

----------------------------Funcionario OCI----------------------------
    select
        1 / case
            when count(*) = 12 then 1
            else 0
        end
    from
        public.granted_authorities
    where
        authority = 'funcionario_oci';

----------------------------Funcionario OCI Wifi----------------------------
    select
        1 / case
            when count(*) = 4 then 1
            else 0
        end
    from
        public.granted_authorities
    where
        authority = 'funcionario_oci_wifi';

----------------------------Jefe de Servicio----------------------------
    select
        1 / case
            when count(*) = 132 then 1
            else 0
        end
    from
        public.granted_authorities
    where
        authority = 'jefe_servicio';

----------------------Jefe de Servicio Medioambiente--------------------
    select
        1 / case
            when count(*) = 2 then 1
            else 0
        end
    from
        public.granted_authorities
    where
        authority = 'jefe_servicio_medioambiente';

----------------------Jefes de Servicio Movilidad----------------------
    select
        1 / case
            when count(*) = 2 then 1
            else 0
        end
    from
        public.granted_authorities
    where
        authority = 'jefe_servicio_movilidad';

---------------------------Politico Gobierno---------------------------
    select
        1 / case
            when count(*) = 10 then 1
            else 0
        end
    from
        public.granted_authorities
    where
        authority = 'politico_gobierno';

---------------------------Politico Oposicion---------------------------
    select
        1 / case
            when count(*) = 3 then 1
            else 0
        end
    from
        public.granted_authorities
    where
        authority = 'politico_oposicion';

------------------------------Secretario-------------------------------
    select
        1 / case
            when count(*) = 12 then 1
            else 0
        end
    from
        public.granted_authorities
    where
        authority = 'secretario';

---------------------Eliminar Roles---------------------
    select
        1 / case
            when count(*) = 0 then 1
            else 0
        end
    from
        public.granted_authorities
    where
        authority in (
            'AccEconCiudAlcaMedSen',
            'AccEconCiudMoviAlcaMedSen',
            'AccEconCiudMoviAlcaMedSenWif',
            'AccCiudMoviAlca',
            'AccEconCiudMoviAlca',
            'AccCiudadMovilidad',
            'AccMovilidad',
            'AccEconomico',
            'AccEconMed',
            'AccEconomicoMovilidadCiudad',
            'AccCiudad',
            'AccCiudadAlcaldia'
        );

ROLLBACK;