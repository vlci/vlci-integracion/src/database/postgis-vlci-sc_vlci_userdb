-- Verify sc_vlci_userdb:v_migrations/TLF053-cambio_permisos_rol on pg

BEGIN;

SELECT
    1 / CASE
        WHEN COUNT(*) = 7 THEN 1
        ELSE 0
    END
FROM
    public.authorities
WHERE description LIKE '%Mapa Poblacion%';

ROLLBACK;
