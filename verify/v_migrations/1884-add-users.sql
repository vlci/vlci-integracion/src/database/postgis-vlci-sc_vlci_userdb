-- Verify sc_vlci_userdb:v_migrations/1884-add-users on pg

BEGIN;


select 1/count(*) from public.users WHERE username='U301362';
select 1/count(*) from public.users WHERE username='U18761';

select 1/count(*) from public.granted_authorities WHERE username='U301362';
select 1/count(*) from public.granted_authorities WHERE username='U18761';

ROLLBACK;
