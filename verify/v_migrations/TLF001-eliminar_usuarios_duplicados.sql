-- Verify sc_vlci_userdb:v_migrations/TLF001-eliminar_usuarios_duplicados on pg

BEGIN;

WITH cte AS (
  SELECT 
    CASE 
      WHEN COUNT(*) != 0 THEN 0
      ELSE 1
    END AS count_result
  FROM public.granted_authorities
  WHERE ctid NOT IN (
    SELECT DISTINCT ON (username, authority) ctid
    FROM public.granted_authorities
  )
)
SELECT 
  1/count_result
FROM cte;

ROLLBACK;
