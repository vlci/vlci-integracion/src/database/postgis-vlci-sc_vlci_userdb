-- Verify sc_vlci_userdb:v_migrations/2858-alta-usuario-juan on pg

BEGIN;

select 1/count(*) from public.users WHERE username='ULULOJ';
select 1/count(*) from public.users WHERE username='ululoj';

select 1/verify.outvalue from (
select
    CASE
        WHEN (count(*) = 250) THEN 1
        ELSE 0
    END as outvalue
from public.users
) verify;

select 1/count(*) from public.granted_authorities WHERE username='ULULOJ';
select 1/count(*) from public.granted_authorities WHERE username='ululoj';

select 1/verify.outvalue from (
select
    CASE
        WHEN (count(*) = 250) THEN 1
        ELSE 0
    END as outvalue
from public.granted_authorities
) verify;

ROLLBACK;
