-- Verify sc_vlci_userdb:v_migrations/TLF052-agregar_usuarios_rol on pg

BEGIN;

    SELECT
        1 / CASE
            WHEN COUNT(*) = 236 THEN 1
            ELSE 0
        END
    FROM
        public.users;

    SELECT
        1 / CASE
            WHEN COUNT(*) = 236 THEN 1
            ELSE 0
        END
    FROM
        public.granted_authorities;

    SELECT
        1 / CASE
            WHEN COUNT(*) = 16 THEN 1
            ELSE 0
        END
    FROM
        public.authorities;
ROLLBACK;
