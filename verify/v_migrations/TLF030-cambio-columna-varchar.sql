-- Verify sc_vlci_userdb:v_migrations/TLF030-cambio-columna-varchar on pg

BEGIN;

SELECT 
  1 / CASE WHEN typname = 'varchar' THEN 1 ELSE 0 END AS is_varchar 
FROM 
  pg_attribute 
  JOIN pg_type ON atttypid = pg_type.oid 
WHERE 
  attrelid = 'public.granted_authorities' :: regclass 
  AND attname = 'username';

SELECT 
  1 / CASE WHEN typname = 'varchar' THEN 1 ELSE 0 END AS is_varchar
FROM 
  pg_attribute 
  JOIN pg_type ON atttypid = pg_type.oid 
WHERE 
  attrelid = 'public.users' :: regclass 
  AND attname = 'username';


ROLLBACK;
