-- Verify sc_vlci_userdb:v_migrations/TLF027-cambio_rol on pg

BEGIN;

SELECT 1/
	CASE 
		WHEN COUNT(*) = 16 THEN 1 
		ELSE 0 
	END
	FROM public.authorities;

select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='U19895' and authority='AccEconomicoMovilidadCiudad';
select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='u19895' and authority='AccEconomicoMovilidadCiudad';

ROLLBACK;
