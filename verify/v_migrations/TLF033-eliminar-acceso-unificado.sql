-- Verify sc_vlci_userdb:v_migrations/TLF033-eliminar-acceso-unificado on pg
BEGIN;

SELECT
    1 / CASE
        WHEN COUNT(*) = 11 THEN 1
        ELSE 0
    END
FROM
    public.authorities;

SELECT
    1 / CASE
        WHEN COUNT(*) = 0 THEN 1
        ELSE 0
    END
FROM
    public.granted_authorities
WHERE
    authority = 'AccUnifEconCiudMoviAlca';

ROLLBACK;