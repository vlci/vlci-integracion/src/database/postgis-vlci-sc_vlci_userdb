-- Verify sc_vlci_userdb:v_migrations/TLF013-agregar_rol on pg

BEGIN;

SELECT 1/
  CASE 
    WHEN COUNT(*) = 12 THEN 1 
    ELSE 0 
  END
FROM public.authorities;

ROLLBACK;
