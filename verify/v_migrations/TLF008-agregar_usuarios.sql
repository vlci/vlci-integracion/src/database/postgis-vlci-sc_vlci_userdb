-- Verify sc_vlci_userdb:v_migrations/TLF008-agregar_usuarios on pg

BEGIN;


select 1/count(*) from public.users WHERE username='URUSAS';
select 1/count(*) from public.users WHERE username='urusas';
select 1/count(*) from public.users WHERE username='LJPL';
select 1/count(*) from public.users WHERE username='ljpl';

select 1/count(*) from public.granted_authorities WHERE username='URUSAS';
select 1/count(*) from public.granted_authorities WHERE username='urusas';
select 1/count(*) from public.granted_authorities WHERE username='LJPL';
select 1/count(*) from public.granted_authorities WHERE username='ljpl';

ROLLBACK;
