-- Verify sc_vlci_userdb:v_migrations/TLF002-cambiar_rol_admins on pg

BEGIN;

select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='u301335' and authority='DBPentDeveloper';
select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='U301335' and authority='DBPentDeveloper';
select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='upacna' and authority='DBPentDeveloper';
ROLLBACK;
