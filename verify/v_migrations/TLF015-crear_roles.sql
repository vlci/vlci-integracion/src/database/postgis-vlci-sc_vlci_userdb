-- Verify sc_vlci_userdb:v_migrations/TLF015-crear_roles on pg

BEGIN;

SELECT 1/
	CASE 
		WHEN COUNT(*) = 15 THEN 1 
		ELSE 0 
	END
	FROM public.authorities;

ROLLBACK;
