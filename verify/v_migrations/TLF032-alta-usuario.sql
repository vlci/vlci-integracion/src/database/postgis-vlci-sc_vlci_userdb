-- Verify sc_vlci_userdb:v_migrations/TLF032-alta-usuario on pg

BEGIN;

SELECT 1/
	CASE 
		WHEN COUNT(*) = 246 THEN 1 
		ELSE 0 
	END
	FROM public.users;

SELECT 1/
	CASE 
		WHEN COUNT(*) = 246 THEN 1 
		ELSE 0 
	END
	FROM public.granted_authorities;

ROLLBACK;
