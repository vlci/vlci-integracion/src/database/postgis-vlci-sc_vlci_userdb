-- Verify sc_vlci_userdb:v_migrations/TLF046-cambiar_rol_usuario on pg

BEGIN;

select
    1 / case
        when count(*) = 4 then 1
        else 0
    end
from
    public.granted_authorities
where
    authority = 'AccEconCiudMoviAlca';

ROLLBACK;
