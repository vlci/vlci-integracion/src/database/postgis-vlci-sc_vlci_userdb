-- Verify sc_vlci_userdb:v_migrations/TLF020-modificar_rol_alcaldia on pg

BEGIN;

	select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='LEFC' and authority='AccUnifEconCiudMoviAlca';
	select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='lefc' and authority='AccUnifEconCiudMoviAlca';
	select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='LRFT' and authority='AccUnifEconCiudMoviAlca';
	select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='lrft' and authority='AccUnifEconCiudMoviAlca';
	select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='U16655' and authority='AccUnifEconCiudMoviAlca';
	select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='u16655' and authority='AccUnifEconCiudMoviAlca';
	select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='U17625' and authority='AccUnifEconCiudMoviAlca';
	select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='u17625' and authority='AccUnifEconCiudMoviAlca';
	select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='U16006' and authority='AccUnifEconCiudMoviAlca';
	select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='u16006' and authority='AccUnifEconCiudMoviAlca';
	select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='U18805' and authority='AccUnifEconCiudMoviAlca';
	select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='u18805' and authority='AccUnifEconCiudMoviAlca';
	select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='U14585' and authority='AccUnifEconCiudMoviAlca';
	select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='u14585' and authority='AccUnifEconCiudMoviAlca';


ROLLBACK;
