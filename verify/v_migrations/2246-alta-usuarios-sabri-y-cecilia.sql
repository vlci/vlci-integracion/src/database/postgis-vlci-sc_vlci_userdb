-- Verify sc_vlci_userdb:v_migrations/2246-alta-usuarios-sabri-y-cecilia on pg

BEGIN;

select 1/count(*) from public.users WHERE username='UMAMAS';
select 1/count(*) from public.users WHERE username='umamas';
select 1/count(*) from public.users WHERE username='UFIROC';
select 1/count(*) from public.users WHERE username='ufiroc';

select 1/verify.outvalue from (
select
    CASE
        WHEN (count(*) = 265) THEN 1
        ELSE 0
    END as outvalue
from public.users
) verify;

select 1/count(*) from public.granted_authorities WHERE username='UMAMAS';
select 1/count(*) from public.granted_authorities WHERE username='umamas';
select 1/count(*) from public.granted_authorities WHERE username='UFIROC';
select 1/count(*) from public.granted_authorities WHERE username='ufiroc';

select 1/verify.outvalue from (
select
    CASE
        WHEN (count(*) = 265) THEN 1
        ELSE 0
    END as outvalue
from public.granted_authorities
) verify;

ROLLBACK;
