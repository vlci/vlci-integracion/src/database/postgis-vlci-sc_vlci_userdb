-- Verify sc_vlci_userdb:v_migrations/2905-alta-usuario on pg

BEGIN;

SELECT 1/
	CASE 
		WHEN COUNT(*) = 254 THEN 1 
		ELSE 0 
	END
	FROM public.users;

SELECT 1/
	CASE 
		WHEN COUNT(*) = 254 THEN 1 
		ELSE 0 
	END
	FROM public.granted_authorities;
ROLLBACK;
