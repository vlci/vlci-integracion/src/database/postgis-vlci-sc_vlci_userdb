-- Verify sc_vlci_userdb:v_migrations/TLF005-agregar_usuarios on pg

BEGIN;

select 1/count(*) from public.users WHERE username='UBAMOD';
select 1/count(*) from public.users WHERE username='ubamod';
select 1/count(*) from public.users WHERE username='U18133';
select 1/count(*) from public.users WHERE username='u18133';
select 1/count(*) from public.users WHERE username='U16656';
select 1/count(*) from public.users WHERE username='u16656';
select 1/count(*) from public.users WHERE username='U17569';
select 1/count(*) from public.users WHERE username='u17569';
select 1/count(*) from public.users WHERE username='UGUAGJ';
select 1/count(*) from public.users WHERE username='uguagj';

select 1/count(*) from public.granted_authorities WHERE username='UBAMOD';
select 1/count(*) from public.granted_authorities WHERE username='ubamod';
select 1/count(*) from public.granted_authorities WHERE username='U18133';
select 1/count(*) from public.granted_authorities WHERE username='u18133';
select 1/count(*) from public.granted_authorities WHERE username='U16656';
select 1/count(*) from public.granted_authorities WHERE username='u16656';
select 1/count(*) from public.granted_authorities WHERE username='U17569';
select 1/count(*) from public.granted_authorities WHERE username='u17569';
select 1/count(*) from public.granted_authorities WHERE username='UGUAGJ';
select 1/count(*) from public.granted_authorities WHERE username='uguagj';

ROLLBACK;