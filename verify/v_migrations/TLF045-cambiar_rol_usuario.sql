-- Verify sc_vlci_userdb:v_migrations/TLF045-cambiar_rol_usuario on pg

BEGIN;

select
    1 / case
        when count(*) = 2 then 1
        else 0
    end
from
    public.granted_authorities
where
    authority = 'AccCiudadAlcaldia';

ROLLBACK;
