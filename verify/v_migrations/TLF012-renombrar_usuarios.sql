-- Verify sc_vlci_userdb:v_migrations/TLF012-renombrar_usuarios on pg
BEGIN;

SELECT
    1 / COUNT(*)
FROM
    public.users
WHERE
    description = 'Francisco Jose Chillida Marco';

SELECT
    1 / COUNT(*)
FROM
    public.users
WHERE
    description = 'Ramón Ferri Tormo';

SELECT
    1 / COUNT(*)
FROM
    public.users
WHERE
    description = 'Jose Manuel Garcia Garcia';

SELECT
    1 / COUNT(*)
FROM
    public.users
WHERE
    description = 'Gines Buendia Buchon';

SELECT
    1 / COUNT(*)
FROM
    public.users
WHERE
    description = 'Jesus Sánchez Company';

ROLLBACK;