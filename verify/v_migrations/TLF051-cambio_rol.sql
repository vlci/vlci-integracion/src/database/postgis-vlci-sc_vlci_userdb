-- Verify sc_vlci_userdb:v_migrations/TLF051-cambio_rol on pg

BEGIN;

    select
        1 / case
            when count(*) = 12 then 1
            else 0
        end
    from
        public.granted_authorities
    where
        authority = 'politico_gobierno';

ROLLBACK;
