-- Verify sc_vlci_userdb:v_migrations/TLF004-cambiar_rol_usuarios on pg

BEGIN;

select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='U301362' and authority='AccUnifEconCiudMoviAlca';
select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='u301362' and authority='AccUnifEconCiudMoviAlca';
select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='U18761' and authority='AccUnifEconCiudMoviAlca';
select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='u18761' and authority='AccUnifEconCiudMoviAlca';
select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='U18667' and authority='AccUnifEconCiudMoviAlca';
select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='u18667' and authority='AccUnifEconCiudMoviAlca';
select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='U13580' and authority='AccUnifEconCiudMoviAlca';
select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='u13580' and authority='AccUnifEconCiudMoviAlca';
ROLLBACK;
