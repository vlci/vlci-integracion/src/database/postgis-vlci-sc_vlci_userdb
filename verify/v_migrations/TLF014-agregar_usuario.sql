-- Verify sc_vlci_userdb:v_migrations/TLF014-agregar_usuario on pg

BEGIN;

SELECT 1/
  CASE 
    WHEN COUNT(*) = 267 THEN 1 
    ELSE 0 
  END
FROM public.granted_authorities;

SELECT 1/
  CASE 
    WHEN COUNT(*) = 267 THEN 1 
    ELSE 0 
  END
FROM public.users;

ROLLBACK;
