-- Verify sc_vlci_userdb:v_migrations/TLF047-eliminar_acceso_med_sen on pg
BEGIN;

select
    1 / case
        when count(*) = 10 then 1
        else 0
    end
from
    public.granted_authorities
where
    authority = 'AccCiudadAlcaldia';

select
    1 / case
        when count(*) = 12 then 1
        else 0
    end
from
    public.granted_authorities
where
    authority = 'AccCiudMoviAlca';

ROLLBACK;