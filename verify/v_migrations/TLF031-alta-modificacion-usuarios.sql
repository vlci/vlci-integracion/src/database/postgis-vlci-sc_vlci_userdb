-- Verify sc_vlci_userdb:v_migrations/TLF031-alta-modificacion-usuarios on pg

BEGIN;


SELECT 1/
	CASE 
		WHEN COUNT(*) = 17 THEN 1 
		ELSE 0 
	END
	FROM public.authorities;

select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='U66173' and authority='AccCiudadMovilidad';
select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='u66173' and authority='AccCiudadMovilidad';
select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='U17994' and authority='AccCiudadMovilidad';
select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='u17994' and authority='AccCiudadMovilidad';
select 1/count(*) from sc_vlci_userdb.public.granted_authorities where username='u19895' and authority='AccEconomicoMovilidadCiudad';

SELECT 1/
	CASE 
		WHEN COUNT(*) = 244 THEN 1 
		ELSE 0 
	END
	FROM public.users;

SELECT 1/
	CASE 
		WHEN COUNT(*) = 244 THEN 1 
		ELSE 0 
	END
	FROM public.granted_authorities;
ROLLBACK;
