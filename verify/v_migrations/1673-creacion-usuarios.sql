-- Verify sc_vlci_userdb:v_migrations/1673-creacion-usuarios on pg

BEGIN;

select 1/verify.outvalue from (
select
    CASE
        WHEN (count(*) = 333) THEN 1
        ELSE 0
    END as outvalue
from  sc_vlci_userdb.public.users
) verify;
select 1/verify.outvalue from (
select
    CASE
        WHEN (count(*) = 337) THEN 1
        ELSE 0
    END as outvalue
from  sc_vlci_userdb.public.granted_authorities
) verify;


ROLLBACK;
